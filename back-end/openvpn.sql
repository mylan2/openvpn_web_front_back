-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 27, 2024 at 10:52 AM
-- Server version: 8.0.36-0ubuntu0.22.04.1
-- PHP Version: 8.1.2-1ubuntu2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `openvpn`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int NOT NULL,
  `client_name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `client_key` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `client_cert` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `client_config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `client_deleted` int NOT NULL DEFAULT '0',
  `client_revoked` int NOT NULL DEFAULT '0',
  `server_id` int NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`client_id`, `client_name`, `client_key`, `client_cert`, `client_config`, `client_deleted`, `client_revoked`, `server_id`, `created_at`) VALUES
(3, 'client16_server01_mNWDikRTuf', '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDHgFGP9mDWyWt/\nQinOp/NjjVv6oxlsJ/LklHd5Ty8Bl+nVNRGsgUCgQmsmIdkdTw2ZT7rwDCZqGIFr\nLuMe8L1CuohmOHS4h2+LUyGrrYantOgDfTUmnbvSnBgvezNujuprFB7QBfjUGd55\nv62iqSkkFhWVh3m9D9UHg9/jnnTXWohEq5hRqD3Iai4rL3cbxjSSkb7+RppT8NRK\nByJOTQ4B77AVsV68dp5SUYAbu3uH7/vrWW7wTZUcu5WS5fBSAjE5Yulz+ffYNzpk\nJYDEXdsHJwjIX6Y1hMRWDhgY0BVggRmiyR6DNfQkCPDVRVRolHVjmm0c/04Rs0Q6\ngzSFSKRvAgMBAAECggEAMG7kHj2zw+FRcis3aB7ZFV2fq2kGRn4qVzj+ClrhcBMX\n5zWDjWwxqtaFGdRU1+s1w3caTKW+e7poTQOsivnqELWlYJ+rIpNqzpnHjlzJ2JQw\nUYyHp1Da5OYVbw2LbndOzOEtG9afUqrXGpqgxmEoBkVARzWFJhOeZb0r0SBKgG9h\nIUsPbW2szV8HuqOqZT8jcRUkBrNEnml/93qrv3mxTYZ2Q32nZ0M9sq/AnTmu83Ka\nUV+a+UN3JAXzE23geodQgfghVu97YkEPk9MrRmTWwbdZnVn+6p9QGZNWGZAx/YDC\n0Wvegf99JsVLkGuEeZkacsqVpYw1b7IoieQXjwwLpQKBgQDH985c+l+unKr1NiCO\nZTP9GGbPTQg8Nh9Nhp3vHEgDxXxEIiR2RM6xFsHoGmm6ntpxLSuzzEL7IwgI4lgq\naryqsf6cBWniyRVUCJWueFhbxcOkXyOqYqiD2xmtWToRazyFW8IG4B0KDnGu/KKQ\ntjeOpzwn7yaxa+e5UcCR6RT/VQKBgQD/ZwgRYN8V6GeqyIPHOSYnOQao3eFvSBfR\nSuUEVsUsnfGowZmva/CrlBzwim7+iofG1uTk31Beh/OeTVf2JprsMGIFUMA8CIlS\nL8j0mY6vox4BvnH4KUrLb3OQvx1Y/ZUWor7p6VSKol8GeoxWL7HZBVHdp2RxPQP1\nXCGt/dmsswKBgQCDR2OaVOEWEFKH80YCVdyAr1FnjOn4JYv9aIAs50M7/p2rWULY\nxilvx0H778MzunAsVvxW6KWcnA39TyOUjUhZQ8ukgEVYlqzC43Po+6b+cOIU+kS+\n6lA5aFD+JhLiNqWf43KI8v79iCgIOmE7YfUtWQs8KFwdpHqXhl1HLV8UlQKBgQCC\n/Qi6QQCpb8SbZrOPXdQfkbPEr+eK92KfyNWkYAxIUck7T+/OdIG+vfwJsjYy/kGc\nzWIfsBi8IuXjbq+KY0pZkIb1SvEM2SP0ruVV7dusYPBm3JH/QM5HgCW5OwuGNKhe\nL6HSvgTKBXTPEdMf8N45++x8zflJyo1ZLaNoZSVa2QKBgBH0gWPwL3TdwAmkV7qF\nVm3Rfv8iohRj1oA1F4AyIttk05/s6iZWkZ7wbMeWewjFhrhh8FRp4kYBtPFq7DOk\n4n8S7+attErPhTU5HGHtF5XBGoEv39XjwEqbaIzJzZjH0WIAK8CwqMrwnE/BLFnY\nKvjow0dPQukTRE7uonIAp8wd\n-----END PRIVATE KEY-----\n', 'Certificate:\n    Data:\n        Version: 3 (0x2)\n        Serial Number:\n            19:e6:a3:d4:4d:75:bb:25:a1:1d:81:9f:4a:b4:cc:0e\n        Signature Algorithm: sha256WithRSAEncryption\n        Issuer: CN=ChangeMe\n        Validity\n            Not Before: Feb 27 03:15:45 2024 GMT\n            Not After : Feb 22 03:15:45 2044 GMT\n        Subject: CN=client16_server01_mNWDikRTuf\n        Subject Public Key Info:\n            Public Key Algorithm: rsaEncryption\n                Public-Key: (2048 bit)\n                Modulus:\n                    00:c7:80:51:8f:f6:60:d6:c9:6b:7f:42:29:ce:a7:\n                    f3:63:8d:5b:fa:a3:19:6c:27:f2:e4:94:77:79:4f:\n                    2f:01:97:e9:d5:35:11:ac:81:40:a0:42:6b:26:21:\n                    d9:1d:4f:0d:99:4f:ba:f0:0c:26:6a:18:81:6b:2e:\n                    e3:1e:f0:bd:42:ba:88:66:38:74:b8:87:6f:8b:53:\n                    21:ab:ad:86:a7:b4:e8:03:7d:35:26:9d:bb:d2:9c:\n                    18:2f:7b:33:6e:8e:ea:6b:14:1e:d0:05:f8:d4:19:\n                    de:79:bf:ad:a2:a9:29:24:16:15:95:87:79:bd:0f:\n                    d5:07:83:df:e3:9e:74:d7:5a:88:44:ab:98:51:a8:\n                    3d:c8:6a:2e:2b:2f:77:1b:c6:34:92:91:be:fe:46:\n                    9a:53:f0:d4:4a:07:22:4e:4d:0e:01:ef:b0:15:b1:\n                    5e:bc:76:9e:52:51:80:1b:bb:7b:87:ef:fb:eb:59:\n                    6e:f0:4d:95:1c:bb:95:92:e5:f0:52:02:31:39:62:\n                    e9:73:f9:f7:d8:37:3a:64:25:80:c4:5d:db:07:27:\n                    08:c8:5f:a6:35:84:c4:56:0e:18:18:d0:15:60:81:\n                    19:a2:c9:1e:83:35:f4:24:08:f0:d5:45:54:68:94:\n                    75:63:9a:6d:1c:ff:4e:11:b3:44:3a:83:34:85:48:\n                    a4:6f\n                Exponent: 65537 (0x10001)\n        X509v3 extensions:\n            X509v3 Basic Constraints: \n                CA:FALSE\n            X509v3 Subject Key Identifier: \n                0E:AF:08:10:DC:90:C6:84:BF:B9:AA:E5:48:75:D1:6F:CF:E7:67:19\n            X509v3 Authority Key Identifier: \n                keyid:74:6B:E6:A6:AC:89:65:B3:64:BB:25:86:E8:B1:10:10:73:D2:0C:08\n                DirName:/CN=ChangeMe\n                serial:49:4C:50:A3:B2:6D:99:DE:06:C4:D2:95:2B:40:6E:31:C2:03:69:B4\n            X509v3 Extended Key Usage: \n                TLS Web Client Authentication\n            X509v3 Key Usage: \n                Digital Signature\n    Signature Algorithm: sha256WithRSAEncryption\n    Signature Value:\n        65:72:f3:f0:48:91:bc:78:2d:8f:35:71:a1:57:58:8b:af:a9:\n        43:ad:a0:f1:28:81:e5:21:b5:09:92:e7:4a:9a:8b:27:26:e7:\n        be:31:54:18:26:0c:34:76:a8:55:9d:ff:9a:c3:fa:4c:53:0e:\n        b6:c0:97:99:4d:df:f9:58:f0:12:c5:39:d6:6d:41:7f:67:d4:\n        c4:b8:b9:4e:b5:72:52:6a:97:38:10:d5:3a:24:08:50:42:0b:\n        4e:a4:1c:01:a4:d6:b4:2c:86:15:71:b0:f1:a8:83:c0:67:c3:\n        47:fb:03:65:90:9b:b0:82:34:1c:68:44:b2:08:ee:36:57:0d:\n        95:4d:d1:09:36:87:c9:08:3b:7f:ae:33:00:a7:19:ca:15:f1:\n        1d:95:0a:1a:b4:58:fb:7b:24:59:50:b2:a0:2a:a0:b0:e7:cb:\n        1b:34:ef:0b:96:99:bf:0c:80:12:1a:0a:10:c6:17:9e:79:63:\n        65:39:4c:eb:33:6d:c3:9c:49:d1:ce:6f:64:d9:b3:3d:03:b8:\n        15:d5:ea:5b:0f:30:37:09:cf:1a:5c:c2:c2:77:21:f3:96:0a:\n        ab:22:e6:b8:d8:be:7e:fd:9d:99:b5:dd:5f:96:b9:ef:6b:82:\n        14:03:b0:18:61:c0:d9:5f:42:31:b0:9f:cb:22:63:a1:06:bd:\n        eb:4c:a0:cc\n-----BEGIN CERTIFICATE-----\nMIIDZDCCAkygAwIBAgIQGeaj1E11uyWhHYGfSrTMDjANBgkqhkiG9w0BAQsFADAT\nMREwDwYDVQQDDAhDaGFuZ2VNZTAeFw0yNDAyMjcwMzE1NDVaFw00NDAyMjIwMzE1\nNDVaMCcxJTAjBgNVBAMMHGNsaWVudDE2X3NlcnZlcjAxX21OV0Rpa1JUdWYwggEi\nMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHgFGP9mDWyWt/QinOp/NjjVv6\noxlsJ/LklHd5Ty8Bl+nVNRGsgUCgQmsmIdkdTw2ZT7rwDCZqGIFrLuMe8L1Cuohm\nOHS4h2+LUyGrrYantOgDfTUmnbvSnBgvezNujuprFB7QBfjUGd55v62iqSkkFhWV\nh3m9D9UHg9/jnnTXWohEq5hRqD3Iai4rL3cbxjSSkb7+RppT8NRKByJOTQ4B77AV\nsV68dp5SUYAbu3uH7/vrWW7wTZUcu5WS5fBSAjE5Yulz+ffYNzpkJYDEXdsHJwjI\nX6Y1hMRWDhgY0BVggRmiyR6DNfQkCPDVRVRolHVjmm0c/04Rs0Q6gzSFSKRvAgMB\nAAGjgZ8wgZwwCQYDVR0TBAIwADAdBgNVHQ4EFgQUDq8IENyQxoS/uarlSHXRb8/n\nZxkwTgYDVR0jBEcwRYAUdGvmpqyJZbNkuyWG6LEQEHPSDAihF6QVMBMxETAPBgNV\nBAMMCENoYW5nZU1lghRJTFCjsm2Z3gbE0pUrQG4xwgNptDATBgNVHSUEDDAKBggr\nBgEFBQcDAjALBgNVHQ8EBAMCB4AwDQYJKoZIhvcNAQELBQADggEBAGVy8/BIkbx4\nLY81caFXWIuvqUOtoPEogeUhtQmS50qaiycm574xVBgmDDR2qFWd/5rD+kxTDrbA\nl5lN3/lY8BLFOdZtQX9n1MS4uU61clJqlzgQ1TokCFBCC06kHAGk1rQshhVxsPGo\ng8Bnw0f7A2WQm7CCNBxoRLII7jZXDZVN0Qk2h8kIO3+uMwCnGcoV8R2VChq0WPt7\nJFlQsqAqoLDnyxs07wuWmb8MgBIaChDGF555Y2U5TOszbcOcSdHOb2TZsz0DuBXV\n6lsPMDcJzxpcwsJ3IfOWCqsi5rjYvn79nZm13V+Wue9rghQDsBhhwNlfQjGwn8si\nY6EGvetMoMw=\n-----END CERTIFICATE-----\n', 'client\ndev tun\nproto udp\nremote 192.168.188.144 1194\nresolv-retry infinite\nnobind\nuser nobody\ngroup nogroup\npersist-key\npersist-tun\nremote-cert-tls server\ncipher AES-256-GCM \nverb 3\n<ca>\n-----BEGIN CERTIFICATE-----\nMIIDQjCCAiqgAwIBAgIUSUxQo7Jtmd4GxNKVK0BuMcIDabQwDQYJKoZIhvcNAQEL\nBQAwEzERMA8GA1UEAwwIQ2hhbmdlTWUwHhcNMjQwMTI5MDEzMzA3WhcNMzQwMTI2\nMDEzMzA3WjATMREwDwYDVQQDDAhDaGFuZ2VNZTCCASIwDQYJKoZIhvcNAQEBBQAD\nggEPADCCAQoCggEBAM61EMMIR8wJZWUICX30qOnQIsP4OLDEoGr0AFn6LTC0ZEL0\n7VSg3EQ01UWQ/Lfjg5ufVe774xCtI/Bz1wJHVSQvOdg0J3wZFW62KN3WX02HJGMI\ndpMg+ZJmgZor0m50rjWBGv2CpPY5mp9ll8vAdKXMxqDYHLeE0DnVCsCAt/e04tlt\nvZasoLPK9IkD2YdVrSIExNuFQyCZrhoBTcFL4ewowAQ/F+r3cqPUlMSlWr5QfiGN\ntTk7TuHfqmTaxGY5G/DaH+kolXvfzt2EFzXScnV8oJMTUYfSvXFG7UsSaFMnO1SF\nKKRVrSPhzF+4E3EeCOrJb1RBS5SqGksA3umzbn0CAwEAAaOBjTCBijAdBgNVHQ4E\nFgQUdGvmpqyJZbNkuyWG6LEQEHPSDAgwTgYDVR0jBEcwRYAUdGvmpqyJZbNkuyWG\n6LEQEHPSDAihF6QVMBMxETAPBgNVBAMMCENoYW5nZU1lghRJTFCjsm2Z3gbE0pUr\nQG4xwgNptDAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG9w0BAQsF\nAAOCAQEAL53uJNRMYiHBrUzrEKX0Vzwil9MqXilL7rnfKknkqaLpqP1/Yn2XmWq8\nBxQ1PntY4CpzX9iCdspejirgWG+gWB2EnBZfptcQGrRBLtEpkSFKjpfgFfxc133c\nGNqlBD80aYbCvmfpojwN48TMXRhS8XGtkwH03LSRnTlL82hwV+orgRthhvLsdtie\n4VXKh0TIwCgJSuxIsXsrQDf19N3kQ9+GAyuVYcjqe5FNDmsEoXdv1mR+y43kwr15\nwrwQKgiYq4IolQcHrQcG8G3dv+gE6sBHK5TUP+7Gtwc8O6hR49nxVsPvdZwejGxY\nRU7eC9EIQYIXyNc/65AOcJzahGnmvA==\n-----END CERTIFICATE-----\n\n</ca>\n<cert>\nCertificate:\n    Data:\n        Version: 3 (0x2)\n        Serial Number:\n            19:e6:a3:d4:4d:75:bb:25:a1:1d:81:9f:4a:b4:cc:0e\n        Signature Algorithm: sha256WithRSAEncryption\n        Issuer: CN=ChangeMe\n        Validity\n            Not Before: Feb 27 03:15:45 2024 GMT\n            Not After : Feb 22 03:15:45 2044 GMT\n        Subject: CN=client16_server01_mNWDikRTuf\n        Subject Public Key Info:\n            Public Key Algorithm: rsaEncryption\n                Public-Key: (2048 bit)\n                Modulus:\n                    00:c7:80:51:8f:f6:60:d6:c9:6b:7f:42:29:ce:a7:\n                    f3:63:8d:5b:fa:a3:19:6c:27:f2:e4:94:77:79:4f:\n                    2f:01:97:e9:d5:35:11:ac:81:40:a0:42:6b:26:21:\n                    d9:1d:4f:0d:99:4f:ba:f0:0c:26:6a:18:81:6b:2e:\n                    e3:1e:f0:bd:42:ba:88:66:38:74:b8:87:6f:8b:53:\n                    21:ab:ad:86:a7:b4:e8:03:7d:35:26:9d:bb:d2:9c:\n                    18:2f:7b:33:6e:8e:ea:6b:14:1e:d0:05:f8:d4:19:\n                    de:79:bf:ad:a2:a9:29:24:16:15:95:87:79:bd:0f:\n                    d5:07:83:df:e3:9e:74:d7:5a:88:44:ab:98:51:a8:\n                    3d:c8:6a:2e:2b:2f:77:1b:c6:34:92:91:be:fe:46:\n                    9a:53:f0:d4:4a:07:22:4e:4d:0e:01:ef:b0:15:b1:\n                    5e:bc:76:9e:52:51:80:1b:bb:7b:87:ef:fb:eb:59:\n                    6e:f0:4d:95:1c:bb:95:92:e5:f0:52:02:31:39:62:\n                    e9:73:f9:f7:d8:37:3a:64:25:80:c4:5d:db:07:27:\n                    08:c8:5f:a6:35:84:c4:56:0e:18:18:d0:15:60:81:\n                    19:a2:c9:1e:83:35:f4:24:08:f0:d5:45:54:68:94:\n                    75:63:9a:6d:1c:ff:4e:11:b3:44:3a:83:34:85:48:\n                    a4:6f\n                Exponent: 65537 (0x10001)\n        X509v3 extensions:\n            X509v3 Basic Constraints: \n                CA:FALSE\n            X509v3 Subject Key Identifier: \n                0E:AF:08:10:DC:90:C6:84:BF:B9:AA:E5:48:75:D1:6F:CF:E7:67:19\n            X509v3 Authority Key Identifier: \n                keyid:74:6B:E6:A6:AC:89:65:B3:64:BB:25:86:E8:B1:10:10:73:D2:0C:08\n                DirName:/CN=ChangeMe\n                serial:49:4C:50:A3:B2:6D:99:DE:06:C4:D2:95:2B:40:6E:31:C2:03:69:B4\n            X509v3 Extended Key Usage: \n                TLS Web Client Authentication\n            X509v3 Key Usage: \n                Digital Signature\n    Signature Algorithm: sha256WithRSAEncryption\n    Signature Value:\n        65:72:f3:f0:48:91:bc:78:2d:8f:35:71:a1:57:58:8b:af:a9:\n        43:ad:a0:f1:28:81:e5:21:b5:09:92:e7:4a:9a:8b:27:26:e7:\n        be:31:54:18:26:0c:34:76:a8:55:9d:ff:9a:c3:fa:4c:53:0e:\n        b6:c0:97:99:4d:df:f9:58:f0:12:c5:39:d6:6d:41:7f:67:d4:\n        c4:b8:b9:4e:b5:72:52:6a:97:38:10:d5:3a:24:08:50:42:0b:\n        4e:a4:1c:01:a4:d6:b4:2c:86:15:71:b0:f1:a8:83:c0:67:c3:\n        47:fb:03:65:90:9b:b0:82:34:1c:68:44:b2:08:ee:36:57:0d:\n        95:4d:d1:09:36:87:c9:08:3b:7f:ae:33:00:a7:19:ca:15:f1:\n        1d:95:0a:1a:b4:58:fb:7b:24:59:50:b2:a0:2a:a0:b0:e7:cb:\n        1b:34:ef:0b:96:99:bf:0c:80:12:1a:0a:10:c6:17:9e:79:63:\n        65:39:4c:eb:33:6d:c3:9c:49:d1:ce:6f:64:d9:b3:3d:03:b8:\n        15:d5:ea:5b:0f:30:37:09:cf:1a:5c:c2:c2:77:21:f3:96:0a:\n        ab:22:e6:b8:d8:be:7e:fd:9d:99:b5:dd:5f:96:b9:ef:6b:82:\n        14:03:b0:18:61:c0:d9:5f:42:31:b0:9f:cb:22:63:a1:06:bd:\n        eb:4c:a0:cc\n-----BEGIN CERTIFICATE-----\nMIIDZDCCAkygAwIBAgIQGeaj1E11uyWhHYGfSrTMDjANBgkqhkiG9w0BAQsFADAT\nMREwDwYDVQQDDAhDaGFuZ2VNZTAeFw0yNDAyMjcwMzE1NDVaFw00NDAyMjIwMzE1\nNDVaMCcxJTAjBgNVBAMMHGNsaWVudDE2X3NlcnZlcjAxX21OV0Rpa1JUdWYwggEi\nMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHgFGP9mDWyWt/QinOp/NjjVv6\noxlsJ/LklHd5Ty8Bl+nVNRGsgUCgQmsmIdkdTw2ZT7rwDCZqGIFrLuMe8L1Cuohm\nOHS4h2+LUyGrrYantOgDfTUmnbvSnBgvezNujuprFB7QBfjUGd55v62iqSkkFhWV\nh3m9D9UHg9/jnnTXWohEq5hRqD3Iai4rL3cbxjSSkb7+RppT8NRKByJOTQ4B77AV\nsV68dp5SUYAbu3uH7/vrWW7wTZUcu5WS5fBSAjE5Yulz+ffYNzpkJYDEXdsHJwjI\nX6Y1hMRWDhgY0BVggRmiyR6DNfQkCPDVRVRolHVjmm0c/04Rs0Q6gzSFSKRvAgMB\nAAGjgZ8wgZwwCQYDVR0TBAIwADAdBgNVHQ4EFgQUDq8IENyQxoS/uarlSHXRb8/n\nZxkwTgYDVR0jBEcwRYAUdGvmpqyJZbNkuyWG6LEQEHPSDAihF6QVMBMxETAPBgNV\nBAMMCENoYW5nZU1lghRJTFCjsm2Z3gbE0pUrQG4xwgNptDATBgNVHSUEDDAKBggr\nBgEFBQcDAjALBgNVHQ8EBAMCB4AwDQYJKoZIhvcNAQELBQADggEBAGVy8/BIkbx4\nLY81caFXWIuvqUOtoPEogeUhtQmS50qaiycm574xVBgmDDR2qFWd/5rD+kxTDrbA\nl5lN3/lY8BLFOdZtQX9n1MS4uU61clJqlzgQ1TokCFBCC06kHAGk1rQshhVxsPGo\ng8Bnw0f7A2WQm7CCNBxoRLII7jZXDZVN0Qk2h8kIO3+uMwCnGcoV8R2VChq0WPt7\nJFlQsqAqoLDnyxs07wuWmb8MgBIaChDGF555Y2U5TOszbcOcSdHOb2TZsz0DuBXV\n6lsPMDcJzxpcwsJ3IfOWCqsi5rjYvn79nZm13V+Wue9rghQDsBhhwNlfQjGwn8si\nY6EGvetMoMw=\n-----END CERTIFICATE-----\n\n</cert>\n<key>\n-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDHgFGP9mDWyWt/\nQinOp/NjjVv6oxlsJ/LklHd5Ty8Bl+nVNRGsgUCgQmsmIdkdTw2ZT7rwDCZqGIFr\nLuMe8L1CuohmOHS4h2+LUyGrrYantOgDfTUmnbvSnBgvezNujuprFB7QBfjUGd55\nv62iqSkkFhWVh3m9D9UHg9/jnnTXWohEq5hRqD3Iai4rL3cbxjSSkb7+RppT8NRK\nByJOTQ4B77AVsV68dp5SUYAbu3uH7/vrWW7wTZUcu5WS5fBSAjE5Yulz+ffYNzpk\nJYDEXdsHJwjIX6Y1hMRWDhgY0BVggRmiyR6DNfQkCPDVRVRolHVjmm0c/04Rs0Q6\ngzSFSKRvAgMBAAECggEAMG7kHj2zw+FRcis3aB7ZFV2fq2kGRn4qVzj+ClrhcBMX\n5zWDjWwxqtaFGdRU1+s1w3caTKW+e7poTQOsivnqELWlYJ+rIpNqzpnHjlzJ2JQw\nUYyHp1Da5OYVbw2LbndOzOEtG9afUqrXGpqgxmEoBkVARzWFJhOeZb0r0SBKgG9h\nIUsPbW2szV8HuqOqZT8jcRUkBrNEnml/93qrv3mxTYZ2Q32nZ0M9sq/AnTmu83Ka\nUV+a+UN3JAXzE23geodQgfghVu97YkEPk9MrRmTWwbdZnVn+6p9QGZNWGZAx/YDC\n0Wvegf99JsVLkGuEeZkacsqVpYw1b7IoieQXjwwLpQKBgQDH985c+l+unKr1NiCO\nZTP9GGbPTQg8Nh9Nhp3vHEgDxXxEIiR2RM6xFsHoGmm6ntpxLSuzzEL7IwgI4lgq\naryqsf6cBWniyRVUCJWueFhbxcOkXyOqYqiD2xmtWToRazyFW8IG4B0KDnGu/KKQ\ntjeOpzwn7yaxa+e5UcCR6RT/VQKBgQD/ZwgRYN8V6GeqyIPHOSYnOQao3eFvSBfR\nSuUEVsUsnfGowZmva/CrlBzwim7+iofG1uTk31Beh/OeTVf2JprsMGIFUMA8CIlS\nL8j0mY6vox4BvnH4KUrLb3OQvx1Y/ZUWor7p6VSKol8GeoxWL7HZBVHdp2RxPQP1\nXCGt/dmsswKBgQCDR2OaVOEWEFKH80YCVdyAr1FnjOn4JYv9aIAs50M7/p2rWULY\nxilvx0H778MzunAsVvxW6KWcnA39TyOUjUhZQ8ukgEVYlqzC43Po+6b+cOIU+kS+\n6lA5aFD+JhLiNqWf43KI8v79iCgIOmE7YfUtWQs8KFwdpHqXhl1HLV8UlQKBgQCC\n/Qi6QQCpb8SbZrOPXdQfkbPEr+eK92KfyNWkYAxIUck7T+/OdIG+vfwJsjYy/kGc\nzWIfsBi8IuXjbq+KY0pZkIb1SvEM2SP0ruVV7dusYPBm3JH/QM5HgCW5OwuGNKhe\nL6HSvgTKBXTPEdMf8N45++x8zflJyo1ZLaNoZSVa2QKBgBH0gWPwL3TdwAmkV7qF\nVm3Rfv8iohRj1oA1F4AyIttk05/s6iZWkZ7wbMeWewjFhrhh8FRp4kYBtPFq7DOk\n4n8S7+attErPhTU5HGHtF5XBGoEv39XjwEqbaIzJzZjH0WIAK8CwqMrwnE/BLFnY\nKvjow0dPQukTRE7uonIAp8wd\n-----END PRIVATE KEY-----\n\n</key>\n<tls-crypt>\n#\n# 2048 bit OpenVPN static key\n#\n-----BEGIN OpenVPN Static key V1-----\ndd2966b477e294f30f5252f6a70e6ce4\ne6f85abae8a00644a9a9b616a9096e49\n1b2c6543cbd0438b2d91d149f1bef3f2\n54a6159c1f18a7de0f4048f8fcbe011c\n17a68cf93ecc2960a33dc1206087ac80\n5603abe36528cc934a5e5303dc0df6dd\n02fe9d2a9d4bfba8af54d7eb8e2136fc\nc1fdbcf03bc20720cc1030f5cf0a0e33\n66ce546501d4d96fda3a6645568d7b40\n52c536bc37612091d55fa9fd8dadbfda\n5c1126acf50054476f3044b21e4f9234\n30c6bbd895b2e695a3b78fadbec88da7\nf5a5ad2d2daa24c7128db5f0b36249eb\n4369c1518e8a4d4459a8779abf18ba4d\na770c28e25907b7ca286fd51d5a06698\n80d2e8ac7eb5f88f3235bd466bbd27f3\n-----END OpenVPN Static key V1-----\n\n</tls-crypt> \n', 0, 1, 1072, '2024-02-27 10:15:46');

-- --------------------------------------------------------

--
-- Table structure for table `servers`
--

CREATE TABLE `servers` (
  `server_id` int NOT NULL,
  `server_name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `server_status` int DEFAULT '1',
  `server_config` text COLLATE utf8mb4_general_ci NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `servers`
--

INSERT INTO `servers` (`server_id`, `server_name`, `server_status`, `server_config`, `create_at`) VALUES
(1057, 'server02', 1, 'mode server\ntls-server\nmanagement 192.168.188.144 8899\nlocal 192.168.188.144\nport 1199\nproto udp\ndev tun\nca server/ca.crt\ncert server/server.crt\nkey server/server.key\ndh server/dh.pem\ncrl-verify server/crl.pem\ntopology subnet\npush \'topology subnet\'\nifconfig 172.16.0.1 255.255.0.0\npush \'route-gateway 172.16.0.1\'\nifconfig-pool 172.16.0.10 10.8.254.254 255.255.0.0\nifconfig-pool-persist server/server02.ipp.txt\nclient-config-dir server/server02.ccd \npush \'dhcp-option DNS 8.8.8.8\'\npush \'dhcp-option DNS 1.1.1.1\'\nclient-to-client\nkeepalive 10 120\ntls-crypt server/ta.key\ncipher AES-256-GCM\nauth SHA256\nuser nobody\ngroup nogroup\npersist-key\npersist-tun\nstatus /var/log/openvpn/openvpn-server02-status.log\nlog /var/log/openvpn/openvpn-server02.log\nlog-append  /var/log/openvpn/openvpn-server02.log\nverb 3\nexplicit-exit-notify 1', '2024-02-07 14:27:39'),
(1072, 'server01', 1, 'mode server\ntls-server\nmanagement 192.168.188.144 8989\nlocal 192.168.188.144\nport 1194\nproto udp\ndev tun\nca server/ca.crt\ncert server/server.crt\nkey server/server.key\ndh server/dh.pem\ncrl-verify server/crl.pem\ntopology subnet\npush \'topology subnet\'\nifconfig 10.8.0.1 255.255.255.0\npush \'route-gateway 10.8.0.1\'\nifconfig-pool 10.8.0.10 10.8.0.254 255.255.255.0\nifconfig-pool-persist server/server01.ipp.txt\nclient-config-dir server/server01.ccd \npush \'dhcp-option DNS 8.8.8.8\'\npush \'dhcp-option DNS 1.1.1.1\'\nclient-to-client\nkeepalive 10 120\ntls-crypt server/ta.key\ncipher AES-256-GCM\nauth SHA256\nuser nobody\ngroup nogroup\npersist-key\npersist-tun\nstatus /var/log/openvpn/openvpn-server01-status.log\nlog /var/log/openvpn/openvpn-server01.log\nlog-append  /var/log/openvpn/openvpn-server01.log\nverb 3\nexplicit-exit-notify 1', '2024-02-16 08:29:15');

-- --------------------------------------------------------

--
-- Table structure for table `server_keys`
--

CREATE TABLE `server_keys` (
  `server_key_id` int NOT NULL,
  `server_key` text COLLATE utf8mb4_general_ci NOT NULL,
  `server_cert` text COLLATE utf8mb4_general_ci NOT NULL,
  `server_ca` text COLLATE utf8mb4_general_ci NOT NULL,
  `server_dh` text COLLATE utf8mb4_general_ci NOT NULL,
  `server_tls` text COLLATE utf8mb4_general_ci NOT NULL,
  `server_crl` text COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `server_keys`
--

INSERT INTO `server_keys` (`server_key_id`, `server_key`, `server_cert`, `server_ca`, `server_dh`, `server_tls`, `server_crl`, `created_at`) VALUES
(1, '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC3CHogEejHSBbx\njMZib4JTS+AkdEiLhUQ1BM9Y6/C3/+tUp+g4Z8PM1lZqh7RHCp+Xtk9GGQQEueiF\nELl8NWSaYN6ii1SSMCsHGajilTOCXoM1+7siF0Cc9CxpDFxKtp+VjKHUVLvy52Tl\nA5IfpkxJnzenKPHMkUheOJ1b5ijd64le1wwztFwbiSHC19xH2+6iKrtFYRgkWWrK\nnWDmZmuhfL6t+FyCCZeqi2mp2tYyFZXX1dlNm3IYhKLXNw5aFKFIC0ChXnk/oIBM\nODJyA7SftFHP0Jm8yGcnmMeW35nTrp53yAnH2+G0ftiWsy0Q3Mjp3DYDPZpzh9Nd\ngXLTdvIhAgMBAAECgf9aLLL+WdR1We7z0odqt8VdhGRJkYCsPFv+QwBkrtlJzawD\naTXflD/DC1ZH5dPLrFA0Tn1/65MvQOhtpm+74hJOfKszKbHg1BpexSWPfDKEaEv8\nrKjlgs8yhJncp1pQ9OhqpX6VPanbYmUy7y9uqF+tZePlVybJwkoFIZ9lR1+a9TrM\nuvbWPGdI49Au2XWHGnkWyMqdFnVrSyQ+R6AWD5TaiXWeZBZgvkvytMe3mr+Zj+6R\nsv0Pjl2IALnCQgoP6Tm4uVJFSnE9UOx7iu6/OIo2wLrHj3ylDaT6wKHdcfjzJ5Or\nkRrbmHjW/UixVymU+1+O9NXnUZBj/n37JIhe2RkCgYEA1y/q8mS1hTRNd4K0ZwPy\nen2cuqRBxtYiwYkra42eA09RVnQHJXtibpPQqHo5sd/PcYEBJE6gnsO1soEj1zDW\nJrF4gMbxWvAkwMCVHbf/Hw4u4m6Ixjo/uKApn+CWRgoN0GzQeq7qUT4TyJNU7/D1\nXXA4bLyN7FXSfdf3LIEcnWkCgYEA2b9ePrb+Wghhxwa43Mb03pEhZpOqtwgu7S1O\ntGnpTdqutEdwan39UtE0lTHks0GoxfGV7AhG1jLS+/vJCFW+yHa1Ku6/57MtRamk\nxoBd0VMFK1F3/79YJHDPFjiKGeZLP45athGvsI/eQJxcP9G4UcSUBGwtVefNa+Yv\nq4cdP/kCgYEAoyeHwgUtxDNl0Qle8wSBIvJ+lNzZqoLNk2QHceLMPcIz75LFoFuu\nxBx7BmK1v/Hr/SlMzlR3YCkPaKqVnCFBHdFNhoZQlRyjISpjZUcOvmMolTItre6Y\nRcnaBYhpJ+2Tj8YRRBO/QZ4JrfhXXB2cTB48CeXkzZ48nIHUvP+fzJECgYB0kt1T\n2ghGDyK5uMGTgPhrh2tITVoh70GgPJdcQy9iPOc3IN/wQ08x3Uq3bSSxSz+d6X6I\nBM5XspH90YiI687d55+KQpN+6hAcsXvDwcl4XWfxDwadZsU7jSFxFgfpYb97YSzz\nOHjEn+Eu2gcRK6fwLrvHulpzGUyCrZNr00Q3kQKBgQCKdexPfp+4wdhbMUjLOvvP\nhwWfBQB49m1Sti7D+CIJZfbytDXZumc+jQYoSZeFMFnr5tjHTa1f54LzzmmIu9q4\nay2YZnt0p5Wn+EYhNAg0R2pJbGPWQeWCSFE7Bt4mPxILsAxwH2tU1aX9AXjbMVqz\niMufva3MSwNLM0jkL5wvDg==\n-----END PRIVATE KEY-----', 'Certificate:\n    Data:\n        Version: 3 (0x2)\n        Serial Number:\n            9c:e6:1a:89:7e:6a:6f:49:ba:a5:02:83:cd:a6:cb:3c\n        Signature Algorithm: sha256WithRSAEncryption\n        Issuer: CN=ChangeMe\n        Validity\n            Not Before: Jan 29 01:33:07 2024 GMT\n            Not After : Mar 30 01:33:07 2043 GMT\n        Subject: CN=server\n        Subject Public Key Info:\n            Public Key Algorithm: rsaEncryption\n                Public-Key: (2048 bit)\n                Modulus:\n                    00:b7:08:7a:20:11:e8:c7:48:16:f1:8c:c6:62:6f:\n                    82:53:4b:e0:24:74:48:8b:85:44:35:04:cf:58:eb:\n                    f0:b7:ff:eb:54:a7:e8:38:67:c3:cc:d6:56:6a:87:\n                    b4:47:0a:9f:97:b6:4f:46:19:04:04:b9:e8:85:10:\n                    b9:7c:35:64:9a:60:de:a2:8b:54:92:30:2b:07:19:\n                    a8:e2:95:33:82:5e:83:35:fb:bb:22:17:40:9c:f4:\n                    2c:69:0c:5c:4a:b6:9f:95:8c:a1:d4:54:bb:f2:e7:\n                    64:e5:03:92:1f:a6:4c:49:9f:37:a7:28:f1:cc:91:\n                    48:5e:38:9d:5b:e6:28:dd:eb:89:5e:d7:0c:33:b4:\n                    5c:1b:89:21:c2:d7:dc:47:db:ee:a2:2a:bb:45:61:\n                    18:24:59:6a:ca:9d:60:e6:66:6b:a1:7c:be:ad:f8:\n                    5c:82:09:97:aa:8b:69:a9:da:d6:32:15:95:d7:d5:\n                    d9:4d:9b:72:18:84:a2:d7:37:0e:5a:14:a1:48:0b:\n                    40:a1:5e:79:3f:a0:80:4c:38:32:72:03:b4:9f:b4:\n                    51:cf:d0:99:bc:c8:67:27:98:c7:96:df:99:d3:ae:\n                    9e:77:c8:09:c7:db:e1:b4:7e:d8:96:b3:2d:10:dc:\n                    c8:e9:dc:36:03:3d:9a:73:87:d3:5d:81:72:d3:76:\n                    f2:21\n                Exponent: 65537 (0x10001)\n        X509v3 extensions:\n            X509v3 Basic Constraints: \n                CA:FALSE\n            X509v3 Subject Key Identifier: \n                40:78:37:94:1F:A5:59:2C:4E:71:A5:75:BA:48:6F:6C:79:E9:B3:51\n            X509v3 Authority Key Identifier: \n                keyid:74:6B:E6:A6:AC:89:65:B3:64:BB:25:86:E8:B1:10:10:73:D2:0C:08\n                DirName:/CN=ChangeMe\n                serial:49:4C:50:A3:B2:6D:99:DE:06:C4:D2:95:2B:40:6E:31:C2:03:69:B4\n            X509v3 Extended Key Usage: \n                TLS Web Server Authentication\n            X509v3 Key Usage: \n                Digital Signature, Key Encipherment\n            X509v3 Subject Alternative Name: \n                DNS:server\n    Signature Algorithm: sha256WithRSAEncryption\n    Signature Value:\n        c3:21:ba:72:00:cd:3e:cc:07:4c:b0:74:e7:a6:13:77:85:8f:\n        0c:58:9c:47:2c:ad:0e:02:66:48:79:47:a0:36:68:c5:fd:d4:\n        19:02:81:bb:b3:e5:e5:b7:5c:c9:72:08:81:37:cb:3a:a3:47:\n        e3:c6:dd:fb:0e:d1:3e:73:e3:30:a6:f5:26:c2:d7:ec:0f:eb:\n        47:d1:da:6f:ae:e8:b0:19:71:7e:52:01:55:44:14:4b:74:e8:\n        aa:cc:ec:ae:71:2a:50:52:51:f8:cf:64:12:2d:87:16:2c:ce:\n        44:9e:6c:e7:d0:f3:5e:54:38:e2:b1:07:34:32:da:a1:0a:b8:\n        2c:97:a6:39:0c:e4:56:3f:46:53:16:1e:b1:fa:a8:9a:d0:46:\n        3b:e6:6f:04:d5:fb:cd:91:7e:19:80:ae:ff:9d:8d:65:a9:dd:\n        3a:71:91:c1:a1:06:99:bc:26:39:39:55:34:e4:ba:25:80:0e:\n        d9:a9:69:7a:94:eb:f1:a8:3e:25:08:b9:78:66:af:2e:ff:5d:\n        31:e7:87:01:86:1b:a3:72:fc:eb:79:54:09:20:3d:27:ef:71:\n        0b:23:a8:f8:4c:87:8d:fc:b4:d4:fb:84:92:49:36:e0:e9:5e:\n        9f:0c:7f:67:73:ad:0a:69:f7:3c:26:1f:71:a3:22:53:a3:6e:\n        47:7a:36:fe\n-----BEGIN CERTIFICATE-----\nMIIDYjCCAkqgAwIBAgIRAJzmGol+am9JuqUCg82myzwwDQYJKoZIhvcNAQELBQAw\nEzERMA8GA1UEAwwIQ2hhbmdlTWUwHhcNMjQwMTI5MDEzMzA3WhcNNDMwMzMwMDEz\nMzA3WjARMQ8wDQYDVQQDDAZzZXJ2ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw\nggEKAoIBAQC3CHogEejHSBbxjMZib4JTS+AkdEiLhUQ1BM9Y6/C3/+tUp+g4Z8PM\n1lZqh7RHCp+Xtk9GGQQEueiFELl8NWSaYN6ii1SSMCsHGajilTOCXoM1+7siF0Cc\n9CxpDFxKtp+VjKHUVLvy52TlA5IfpkxJnzenKPHMkUheOJ1b5ijd64le1wwztFwb\niSHC19xH2+6iKrtFYRgkWWrKnWDmZmuhfL6t+FyCCZeqi2mp2tYyFZXX1dlNm3IY\nhKLXNw5aFKFIC0ChXnk/oIBMODJyA7SftFHP0Jm8yGcnmMeW35nTrp53yAnH2+G0\nftiWsy0Q3Mjp3DYDPZpzh9NdgXLTdvIhAgMBAAGjgbIwga8wCQYDVR0TBAIwADAd\nBgNVHQ4EFgQUQHg3lB+lWSxOcaV1ukhvbHnps1EwTgYDVR0jBEcwRYAUdGvmpqyJ\nZbNkuyWG6LEQEHPSDAihF6QVMBMxETAPBgNVBAMMCENoYW5nZU1lghRJTFCjsm2Z\n3gbE0pUrQG4xwgNptDATBgNVHSUEDDAKBggrBgEFBQcDATALBgNVHQ8EBAMCBaAw\nEQYDVR0RBAowCIIGc2VydmVyMA0GCSqGSIb3DQEBCwUAA4IBAQDDIbpyAM0+zAdM\nsHTnphN3hY8MWJxHLK0OAmZIeUegNmjF/dQZAoG7s+Xlt1zJcgiBN8s6o0fjxt37\nDtE+c+MwpvUmwtfsD+tH0dpvruiwGXF+UgFVRBRLdOiqzOyucSpQUlH4z2QSLYcW\nLM5Enmzn0PNeVDjisQc0MtqhCrgsl6Y5DORWP0ZTFh6x+qia0EY75m8E1fvNkX4Z\ngK7/nY1lqd06cZHBoQaZvCY5OVU05LolgA7ZqWl6lOvxqD4lCLl4Zq8u/10x54cB\nhhujcvzreVQJID0n73ELI6j4TIeN/LTU+4SSSTbg6V6fDH9nc60Kafc8Jh9xoyJT\no25Hejb+\n-----END CERTIFICATE-----', '-----BEGIN CERTIFICATE-----\nMIIDQjCCAiqgAwIBAgIUSUxQo7Jtmd4GxNKVK0BuMcIDabQwDQYJKoZIhvcNAQEL\nBQAwEzERMA8GA1UEAwwIQ2hhbmdlTWUwHhcNMjQwMTI5MDEzMzA3WhcNMzQwMTI2\nMDEzMzA3WjATMREwDwYDVQQDDAhDaGFuZ2VNZTCCASIwDQYJKoZIhvcNAQEBBQAD\nggEPADCCAQoCggEBAM61EMMIR8wJZWUICX30qOnQIsP4OLDEoGr0AFn6LTC0ZEL0\n7VSg3EQ01UWQ/Lfjg5ufVe774xCtI/Bz1wJHVSQvOdg0J3wZFW62KN3WX02HJGMI\ndpMg+ZJmgZor0m50rjWBGv2CpPY5mp9ll8vAdKXMxqDYHLeE0DnVCsCAt/e04tlt\nvZasoLPK9IkD2YdVrSIExNuFQyCZrhoBTcFL4ewowAQ/F+r3cqPUlMSlWr5QfiGN\ntTk7TuHfqmTaxGY5G/DaH+kolXvfzt2EFzXScnV8oJMTUYfSvXFG7UsSaFMnO1SF\nKKRVrSPhzF+4E3EeCOrJb1RBS5SqGksA3umzbn0CAwEAAaOBjTCBijAdBgNVHQ4E\nFgQUdGvmpqyJZbNkuyWG6LEQEHPSDAgwTgYDVR0jBEcwRYAUdGvmpqyJZbNkuyWG\n6LEQEHPSDAihF6QVMBMxETAPBgNVBAMMCENoYW5nZU1lghRJTFCjsm2Z3gbE0pUr\nQG4xwgNptDAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG9w0BAQsF\nAAOCAQEAL53uJNRMYiHBrUzrEKX0Vzwil9MqXilL7rnfKknkqaLpqP1/Yn2XmWq8\nBxQ1PntY4CpzX9iCdspejirgWG+gWB2EnBZfptcQGrRBLtEpkSFKjpfgFfxc133c\nGNqlBD80aYbCvmfpojwN48TMXRhS8XGtkwH03LSRnTlL82hwV+orgRthhvLsdtie\n4VXKh0TIwCgJSuxIsXsrQDf19N3kQ9+GAyuVYcjqe5FNDmsEoXdv1mR+y43kwr15\nwrwQKgiYq4IolQcHrQcG8G3dv+gE6sBHK5TUP+7Gtwc8O6hR49nxVsPvdZwejGxY\nRU7eC9EIQYIXyNc/65AOcJzahGnmvA==\n-----END CERTIFICATE-----', '-----BEGIN DH PARAMETERS-----\nMIIBCAKCAQEAkRm1g78KWcMDAxiWs77TNBeSYMeIDdygSdWh5501oTftakYe+O+G\nq1I7dWE6/hkFTSNXsimPrmR9LI/RKN10PVduAkW/NgTLL7kx5r7z759tx/mr8KWG\ngYc/9AvfPuQd5EK2r3G/4PgPDjqq9Gpz2ukikhadBGtUUTaKN+gRQaWIZzHWAPAJ\nCXKN9bLdtwHUooUzCUHXt7x4acVCzfmbZH7BcZ1+h67A05zrAXteGQlDijxAFaVM\nq12EbfJiTaS6ocfBp20QTbrS5pWjJXMVur9qUfKXcgpRzSKgED854lXOFZTRkCIA\nSmj/jTMU/4fl8URN5fWXYkfpS4ua/R/tbwIBAg==\n-----END DH PARAMETERS-----', '#\n# 2048 bit OpenVPN static key\n#\n-----BEGIN OpenVPN Static key V1-----\ndd2966b477e294f30f5252f6a70e6ce4\ne6f85abae8a00644a9a9b616a9096e49\n1b2c6543cbd0438b2d91d149f1bef3f2\n54a6159c1f18a7de0f4048f8fcbe011c\n17a68cf93ecc2960a33dc1206087ac80\n5603abe36528cc934a5e5303dc0df6dd\n02fe9d2a9d4bfba8af54d7eb8e2136fc\nc1fdbcf03bc20720cc1030f5cf0a0e33\n66ce546501d4d96fda3a6645568d7b40\n52c536bc37612091d55fa9fd8dadbfda\n5c1126acf50054476f3044b21e4f9234\n30c6bbd895b2e695a3b78fadbec88da7\nf5a5ad2d2daa24c7128db5f0b36249eb\n4369c1518e8a4d4459a8779abf18ba4d\na770c28e25907b7ca286fd51d5a06698\n80d2e8ac7eb5f88f3235bd466bbd27f3\n-----END OpenVPN Static key V1-----', '-----BEGIN X509 CRL-----\nMIICiTCCAXECAQEwDQYJKoZIhvcNAQELBQAwEzERMA8GA1UEAwwIQ2hhbmdlTWUX\nDTI0MDIyNjA4MDEwNFoXDTQ0MDIyMTA4MDEwNFowgdUwIQIQSoVS/pEmJ3zG75/4\n+JQxshcNMjQwMjIwMDkxOTE3WjAhAhBQ9rGDuIHngXI94G99fKivFw0yNDAyMjYw\nODAxMDRaMCECEH3RO5Cu5EKqlhKeUK41nsgXDTI0MDIyNjA3NTk0MFowIgIRAMNL\nlKlkSz7xEBzfw2jM8OEXDTI0MDEzMDAyMTkxM1owIgIRAMfVjci6NUgN55PsyYSb\nfXQXDTI0MDIwMTA0MjUzOFowIgIRAPD9C+MWFH0h9h/sGRlC5fcXDTI0MDEzMDA5\nNTY0N1qgUjBQME4GA1UdIwRHMEWAFHRr5qasiWWzZLslhuixEBBz0gwIoRekFTAT\nMREwDwYDVQQDDAhDaGFuZ2VNZYIUSUxQo7Jtmd4GxNKVK0BuMcIDabQwDQYJKoZI\nhvcNAQELBQADggEBAMJZ9RdgvvpBQSWuQ1dY0btJNZijmQIS5SVFZq2hoUvP29IX\ndLTG4dGFL+RSblNDErVkuLHjcdC6rCudiB2dKIWBFMlB5HSYqZ6tXaHUQNfNkIIw\nZRDbqLOcBJagACBEvj2zYjmcaFTOfrQLV46MmwPvQk+N046I5bimZTthuuvy55Z+\nH1zNJzvKlYvHPxIK14WInWElc+DbOnE37a58AXqU8JaFkziPvyVExkzxZfeKq0bA\nBQwmMj8vL6wjH0Bt2IxnL+kpKF/0PP2nKNvoaGdzZyvHwa7+W1uouV4VaqsrEQwJ\njWlgqhBKN5CW4ubgv4WwJaosXHm3Gj4aqDlwUKc=\n-----END X509 CRL-----', '2024-02-27 09:53:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `privilege` int NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `privilege`, `create_at`) VALUES
(1, 'vctrung', '202cb962ac59075b964b07152d234b70', 0, '2024-01-22 02:22:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`,`client_name`) USING BTREE,
  ADD KEY `fk_server_id` (`server_id`);

--
-- Indexes for table `servers`
--
ALTER TABLE `servers`
  ADD PRIMARY KEY (`server_id`,`server_name`);

--
-- Indexes for table `server_keys`
--
ALTER TABLE `server_keys`
  ADD PRIMARY KEY (`server_key_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `servers`
--
ALTER TABLE `servers`
  MODIFY `server_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1073;

--
-- AUTO_INCREMENT for table `server_keys`
--
ALTER TABLE `server_keys`
  MODIFY `server_key_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `fk_server_id` FOREIGN KEY (`server_id`) REFERENCES `servers` (`server_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
