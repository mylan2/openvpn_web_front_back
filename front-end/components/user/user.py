from flask import Blueprint, render_template, request, session
import json 
import hashlib, json

from helpers.init_helpers import connection_db

user_dp = Blueprint('user', __name__,template_folder="templates")

@user_dp.route('/user/<name>', methods=['GET', 'POST'])
def acoount_detail(name):
    if 'user' not in session:
        return render_template('error.html') 
    return render_template('user.html')
##  =========================================== API AREA ========================================================
