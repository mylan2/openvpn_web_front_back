window.onload = function () {
	const sidebar = document.querySelector(".sidebar");
	const closeBtn = document.getElementById("btn");

	closeBtn.addEventListener("click", function () {
		sidebar.classList.toggle("open");
		menuBtnChange();
	});

	function menuBtnChange() {
		if (sidebar.classList.contains("open")) {
			closeBtn.classList.replace("bx-menu", "bx-menu-alt-right");
		} else {
			closeBtn.classList.replace("bx-menu-alt-right", "bx-menu");
		}
	}
};

// Loading pages
function loading(id, status) {
	const loadingData = document.getElementById(id);
	if (loadingData !== null) {
		status
			? loadingData.classList.add("loading")
			: loadingData.classList.remove("loading");
	}
}
//
function debounce(func, delay) {
	let timerId;
	return function (...args) {
		clearTimeout(timerId);
		timerId = setTimeout(() => {
			func.apply(this, args);
		}, delay);
	};
}
//
const searchMessage = "<p>No Data !</p>";
// User info menu
const userMenu = document.getElementById("user-info");
userMenu.addEventListener("click", () => {
	userMenu.classList.toggle("user-info-active");
	const infoUserMenu = document.getElementById("user-info-menu");
	let isActive = infoUserMenu.classList.contains("user-info-menu-active");
	if (isActive) infoUserMenu.classList.remove("user-info-menu-active");
	else infoUserMenu.classList.add("user-info-menu-active");
});
// 

showItemSelecter = document.getElementById("show-item-selecter");
if(showItemSelecter != null) { 
	var clientPerPage = showItemSelecter.value;
	showItemSelecter.addEventListener(
		"change", () => {
			clientPerPage = showItemSelecter.value;
			pagination();
		}
	);
}

function renderPagination(currentPage){
	let itemList = document.querySelectorAll(".items-list .item");
	let totalPage = Math.ceil(itemList.length / clientPerPage);
	document.querySelector(".listPage").innerHTML = "";

	if(currentPage != 1){
		let prev = document.createElement("li");
		prev.innerText = "PREV";
		prev.setAttribute("onclick", "changePage(" + (currentPage - 1) + ")");
		document.querySelector(".listPage").appendChild(prev);
	}
	 
	for(i = 1; i <= totalPage; i++){
		let newPage = document.createElement("li");
		newPage.innerText = i;
		if(i > currentPage + 1 && i < totalPage - 1 || i < currentPage - 1 && i > 2 ) { 
			 newPage.classList.add("d-none");
		} 
		 
		if(i == currentPage){
			newPage.classList.add("active");
		} 
		
		if (i == totalPage - 2 && currentPage < totalPage - 3 || i == 3 && currentPage > 4) {
            let hide = document.createElement("li");
			hide.innerText = "...";
			document.querySelector(".listPage").appendChild(hide);
        }
		 
		newPage.setAttribute("onclick", "changePage(" + i + ")");
		document.querySelector(".listPage").appendChild(newPage);
	}

	if(currentPage != totalPage){
		let next = document.createElement("li");
		next.innerText = "NEXT";
		next.setAttribute("onclick", "changePage(" + (currentPage + 1) + ")");
		document.querySelector(".listPage").appendChild(next);
	} 
}  
// 
function loadItem(itemList, currentPage){
	let startItem = clientPerPage * (currentPage - 1);
	let endItem = clientPerPage * currentPage - 1;
	
	itemList.forEach((item, key)=>{
		if(key >= startItem && key <= endItem){
			item.style.display = 'flex';
		}else{
			item.style.display = 'none';
		}
	})
	renderPagination(currentPage);
}
// 
function changePage(i){
	currentPage = i;
	let itemList = document.querySelectorAll('.items-list .item');
	loadItem(itemList, currentPage);
	
} 
function pagination(){
	let currentPage = 1;
	let itemList = document.querySelectorAll('.items-list .item');
    loadItem(itemList, currentPage);
    
}

// Get data 
getData();
function getData() {
	const itemsList = document.getElementById("items-list");
	if (itemsList == null) {
		return;
	}

	loading("item-content", true);
	let url = "http://192.168.188.144:8080/get-all-client";
	fetch(url, {
		headers: {
			"Content-Type": "application/json; charset=utf-8", 
		},
		// body: JSON.stringify(serverInfo),
	})
		.then((res) => res.json())
		.then((data) => {
			// Get data
			// Debug
			// console.log(data);
			let data_quantity = document.getElementById("data-quantity");
			let data_total = document.getElementById("data-total");
			data_total.innerHTML = data.clients.length;
			if (data.status === false) {
				itemsList.innerHTML = data.message;
				data_quantity.innerHTML = data.clients.length;
			} else {
				let { clients, clientConfig } = data;
				if (clients.length > 0 && clients[0].length > 0) {
					// 
					const txtSearch = document.getElementById("txt-search");
					const tunelFilterSelecter = document.getElementById(
						"tunel-selecter-filter"
					);
					const checkboxFilterRevoke = document.getElementById("checkbox-filter-revoke");
					const checkboxFilterOnOff = document.getElementById("checkbox-filter-onoff");
					const checkboxSort = document.getElementById("checkbox-sort");

					var clientsData = [];
					let searchValue = txtSearch.value.trim().toLowerCase();
					let tunelFilterSelected = tunelFilterSelecter.value.toLowerCase();

					// OnOff
					function filterByOnOff() {
						clientsData = clientsData.filter(
							(client) =>
								clientConfig[client] &&
								clientConfig[client].running_status['connected'] == checkboxFilterOnOff.checked
						);
						return clientsData;
					}
					// Revoke
					function filterByRevoke() {
						clientsData = clientsData.filter(
							(client) =>
								clientConfig[client] && clientConfig[client].revoke == checkboxFilterRevoke.checked
						);
						return clientsData;
					}
					// Filter by server
					function filterByServer() {
						if (tunelFilterSelected == "all") {
							clientsData = clients;
						} else {
							clientsData = clients.filter(
								(client) =>
									clientConfig[client] &&
									clientConfig[client].server
										.toLowerCase()
										.includes(tunelFilterSelected.toLowerCase())
							);
						}
						return clientsData;
					}
					// call filterByServer function 
					filterByServer();
					// call function filterByOnOff and filterByRevoke
					filterByOnOff();
					filterByRevoke();
					// Sort
					function sortClient() {
						if (checkboxSort.checked) {
							clientsData = clientsData.sort((a, b) => {
								if (a.toLowerCase() < b.toLowerCase()) {
									return 1;
								} else if (a.toLowerCase() > b.toLowerCase()) {
									return -1;
								} else {
									return 0;
								}
							});
						} else {
							clientsData = clientsData.sort((a, b) => {
								if (a.toLowerCase() > b.toLowerCase()) {
									return 1;
								} else if (a.toLowerCase() < b.toLowerCase()) {
									return -1;
								} else {
									return 0;
								}
							});
						}
						return clientsData;
					}
					// call sort client function
					sortClient();
					// render result
					function renderResult() {
						if (searchValue.trim().length == 0) {
							clientsData = clientsData.map((client) =>
								renderItem(client, clientConfig)
							);
						} else {
							clientsData = clientsData.filter((client) =>
								client.toLowerCase().includes(searchValue)
							);
							clientsData = clientsData.map((client) =>
								renderItem(client, clientConfig)
							);
						}

						if (clientsData.length == 0) {
							renderClients = searchMessage;
						} else {
							renderClients = clientsData;
						}
						//
						if (Array.isArray(renderClients)) {
							itemsList.innerHTML = renderClients.join("");
							data_quantity.innerHTML = clientsData.length;
							showClientDetails();
						} else {
							itemsList.innerHTML = renderClients;
							data_quantity.innerHTML = "0";
						}
					}
					// call renderResult() function
					renderResult();
					// Call pagination
					pagination();
					// Onchange Checkbox sort
					checkboxSort.addEventListener(
						"change",
						debounce(function () {
							searchValue = txtSearch.value.trim().toLowerCase();
							tunelFilterSelected = tunelFilterSelecter.value.toLowerCase();
							// call filterByServer function 
							filterByServer();
							// call sort client function
							sortClient();
							// call function filterByOnOff 
							filterByOnOff();
							// call function filterByORevoke
							filterByRevoke();
							// call renderResult() function
							renderResult();
							// Call pagination
							pagination()
						}, 500)
					);
					// Onchange Checkbox Filter onofff
					checkboxFilterOnOff.addEventListener(
						"change",
						debounce(function () {
							searchValue = txtSearch.value.trim().toLowerCase();
							tunelFilterSelected = tunelFilterSelecter.value.toLowerCase();
							// call filterByServer function 
							filterByServer();
							// call sort client function
							sortClient();
							// call function filterByOnOff 
							filterByOnOff();
							// call function filterByORevoke
							filterByRevoke();
							// call renderResult() function
							renderResult();
							// Call pagination
							pagination();
						}, 500)
					);
					// Onchange Checkbox Filter revoke
					checkboxFilterRevoke.addEventListener(
						"change",
						debounce(function () {
							searchValue = txtSearch.value.trim().toLowerCase();
							tunelFilterSelected = tunelFilterSelecter.value.toLowerCase();
							// call filterByServer function 
							filterByServer();
							// call sort client function
							sortClient();
							// call function filterByOnOff 
							filterByOnOff();
							// call function filterByORevoke
							filterByRevoke();
							// call renderResult() function
							renderResult();
							// Call pagination
							pagination();
						}, 500)
					);
					// OnKeyUp
					txtSearch.addEventListener(
						"keyup",
						debounce(function () {
							searchValue = this.value.trim().toLowerCase();
							tunelFilterSelected = tunelFilterSelecter.value.toLowerCase();
							// call filterByServer function 
							filterByServer();
							// call sort client function
							sortClient();
							// call function filterByOnOff 
							filterByOnOff();
							// call function filterByORevoke
							filterByRevoke();
							// call renderResult() function
							renderResult();
							// Call pagination
							pagination();
						}, 500)
					);
					// OnSelect change
					tunelFilterSelecter.addEventListener(
						"change",
						debounce(function () {
							tunelFilterSelected = this.value.toLowerCase();
							searchValue = txtSearch.value.trim().toLowerCase();
							// call filterByServer function 
							filterByServer();
							// call sort client function
							sortClient();
							// call function filterByOnOff 
							filterByOnOff();
							// call function filterByORevoke
							filterByRevoke();
							// call renderResult() function
							renderResult();
							// Call pagination
							pagination();
						}, 500)
					);
				} else {
					itemsList.innerHTML = searchMessage;
				}
			}
			setTimeout(() => {
				loading("item-content", false);
			}, 500);
		}).catch(error => {
			console.error('Fetch error:', error);
		});
}
// Get Tunel
getTunel();
function getTunel() {
	const tunelContentDom = document.getElementById("tunel-content");
	const tunelSelecterDom = document.getElementById("tunel-selecter");
	const tunelSelecterFilterDom = document.getElementById(
		"tunel-selecter-filter"
	);
	if (
		(tunelContentDom && tunelSelecterDom && tunelSelecterFilterDom) === null
	) {
		return;
	}
	loading("tunel-detail", true);
	loading("tunel-container", true);
	loading("tunel-selecter-content", true);
	loading("tunel-selecter-filter-content", true);

	let url = 'http://192.168.188.144:8080/tunel/get-all-tunel';
	fetch(url, {
		headers: {
			"Content-Type": "application/json; charset=utf-8",
		}, 
	}) 
		.then((res) =>
			res.json()
		)
		.then((data) => {
			if (data.status == false) {
				tunelContentDom.innerHTML = renderTunelInterface(null);
				tunelSelecterDom.innerHTML = renderTunelSelection(null);
				tunelSelecterFilterDom.innerHTML = renderTunelSelection(null);
			} else {
				let { servers, status } = data.tunels;
				// Render tunel filer tools bar
				if (servers.length == 1 && servers[0] == "") {
					tunelContentDom.innerHTML = renderTunelInterface(null);
					tunelSelecterDom.innerHTML = renderTunelSelection(null);
					tunelSelecterFilterDom.innerHTML = renderTunelSelection(null);
				} else {
					var tunelIsOff = true;
					var renderTunelSelecter = servers.map((tunel) => {
						if (status[tunel].status == "active") {
							tunelIsOff = false;
							return renderTunelSelection(tunel);
						}
					});

					var renderTunelFilterSelecter = servers.map((tunel, index) => {
						if (servers.length == index + 1) {
							return renderTunelSelection(tunel, "All");
						} else {
							return renderTunelSelection(tunel);
						}
					});

					tunelSelecterFilterDom.innerHTML = renderTunelFilterSelecter.join("");
					if (tunelIsOff) {
						tunelSelecterDom.innerHTML = renderTunelSelection(null);
						renderTunel = servers.map((tunel) => renderTunelInterface(tunel));
						tunelContentDom.innerHTML = renderTunel.join("");
					} else {
						tunelSelecterDom.innerHTML = renderTunelSelecter.join("");
						//
						renderTunel = servers.map((tunel) => {
							if (status[tunel].status == "active") {
								return renderTunelInterface(tunel, "power-active");
							} else {
								return renderTunelInterface(tunel);
							}
						});
						tunelContentDom.innerHTML = renderTunel.join("");
					}
					startTunel();
				}
			}
			setTimeout(() => {
				loading("tunel-container", false);
				loading("tunel-selecter-content", false);
				loading("tunel-selecter-filter-content", false);
			}, 500);
		}).catch(error => {
			console.error('Fetch error:', error);
		});
}

// Get tunel Detail
getTunelDetail()
function getTunelDetail() {
	const tunelDetail = document.getElementById("tunel-detail")
	if (tunelDetail != null) {
		loading("tunel-detail", true);
		let sURL = window.document.URL.toString();
		let serverName = sURL.split("/").pop();
		// const serverInfo = {
		// 	'server_name': serverName
		// }
		let url = `http://192.168.188.144:8080/tunel/${serverName}/detail`;
		fetch(url, {
			headers: {
				"Content-Type": "application/json; charset=utf-8",
			},
			// body: JSON.stringify(serverInfo),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data.status) {
					let { tunelConfig } = data;
					tunelDetail.innerHTML = renderTunelInterfaceDetail(tunelConfig);
					startTunel();
				} else {
					tunelDetail.innerHTML = 'No data found for tunel !';
				}
				loading("tunel-detail", false);
			});
	}

}

//======================== Add client========================================
AddClient();
function AddClient() {
	const btnAddClient = document.getElementById("btn-add-client");
	if (btnAddClient == null) {
		return;
	}
	btnAddClient.addEventListener("click", () => {
		let newClientName = document.getElementById("txt-add-client").value.trim();
		let tunelSelected = document.getElementById("tunel-selecter").value.trim();
		if (newClientName.length == 0) {
			Toastify({
                text: "Please enter client name",
                className: "toastify-warning",
            }).showToast();
            return;
		}
		if (tunelSelected.length == 0) {
			Toastify({
                text: "Tunel is not selected",
                className: "toastify-warning",
            }).showToast();
            return;
		}
		// const newClient = {
		// 	name: newClientName,
		// 	tunel: tunelSelected,
		// };
		let url = `http://192.168.188.144:8080/client/add?name=${newClientName}&tunel=${tunelSelected}`;
		fetch(url, {
			method: "POST",
			headers: {
				"Content-Type": "application/json; charset=utf-8",
			},
			// body: JSON.stringify(newClient),
		})
			.then((response) => response.json())
			.then((data) => {
				// debug
				if (data.status) {
					Toastify({
						text: data.message,
						className: "toastify-success",
					}).showToast();
					setTimeout(function () {
						getData();
					}, 2000);
				} else {
					Toastify({
						text: data.message,
						className: "toastify-warning",
					}).showToast();
				}
			});
	});
}

//
startTunel();
function startTunel() {
	const infoItems = document.querySelectorAll(".toggle");
	infoItems.forEach((item) => {
		item.addEventListener("click", () => {

			let itemOff = item.classList.contains("power-off");
			if (itemOff) {
				item.classList.remove("power-off");
				item.classList.add("power-active");
			} else {
				item.classList.add("power-active");
			}
			// let tunelObj = {
			// 	tunel:
			// 		item.getAttribute("data-info").length > 0
			// 			? item.getAttribute("data-info")
			// 			: null,
			// };
			tunel = item.getAttribute("data-info").length > 0
				? item.getAttribute("data-info")
				: null
			let url = `http://192.168.188.144:8080/tunel/${tunel}/up`;
			fetch(url, {
				method: "POST",
				headers: {
					"Content-Type": "application/json; charset=utf-8",
				},
				// body: JSON.stringify(tunelObj),
			})
				.then((response) => response.json())
				.then((data) => {
					if (data.status) {
						Toastify({
							text: data.message,
							className: "toastify-success",
						}).showToast();
						getTunel();
						getTunelDetail();
					} else {
						Toastify({
							text: data.message,
							className: "toastify-warning",
						}).showToast();
					}
				});
		});
	});
}
//
function downLoadFileConfig(client) {
	// let clientName = { client };
	let url = `http://192.168.188.144:8080/client/${client}/profile`;
	fetch(url, {
		headers: {
			"Content-Type": "application/json; charset=utf-8",
		},
		// body: JSON.stringify(clientName),
	})
		.then((response) => response.json())
		.then((data) => {
			if (data.status) {
				if (data.profile && data.profile.length > 0) {
					Toastify({
						text: "This file is being downloaded...",
						className: "toastify-success",
					}).showToast();
					setTimeout(function () {
						let content = data.profile;
						let file = new File(
							["\ufeff" + content],
							client.split("_")[0] + "_" + client.split("_")[1] + ".ovpn",
							{
								type: "text/plain:charset=UTF-8",
							}
						);
						url = window.URL.createObjectURL(file);
						let aTag = document.createElement("a");
						aTag.style = "display: none";
						aTag.href = url;
						aTag.download = file.name;
						aTag.click();
						window.URL.revokeObjectURL(url);
					}, 2000);
				} else {
					Toastify({
						text: "Some thing went wrong !",
						className: "toastify-warning",
					}).showToast();
				}
			} else {
				Toastify({
					text: data.message,
					className: "toastify-warning",
				}).showToast();
			}
		});
}
// Delete client
function del(client) {
	// let clientName = { client };
	if (
		confirm("Are you sure want to Delete " + client.split("_")[0] + " ?") ==
		true
	) {
		let url = `http://192.168.188.144:8080/client/${client}/delete`;
		fetch(url, {
			method: "DELETE",
			headers: {
				"Content-Type": "application/json; charset=utf-8",
			},
			// body: JSON.stringify(clientName),
		})
			.then((response) => response.json())
			.then((data) => {
				if (data.status) {
					Toastify({
						text: data.message,
						className: "toastify-success",
					}).showToast();
					getData();
				} else {
					Toastify({
						text: data.message,
						className: "toastify-warning",
					}).showToast();
				}
			});
	} else {
		Toastify({
			text: "Cancellation successful !",
			className: "toastify-success",
		}).showToast();
	}
}
//
function revoke(client) {
	// let clientName = { client };
	if (
		confirm("Revoke client đồng thời sẽ khởi động lại hệ thống OpenVPN (down time ~ 3s). Are you sure want to Revoke " + client.split("_")[0] + " ?") ==
		true
	) {
		let url = `http://192.168.188.144:8080/client/${client}/revoke`;
		fetch(url, {
			method: "POST",
			headers: {
				"Content-Type": "application/json; charset=utf-8",
			},
			// body: JSON.stringify(clientName),
		})
			.then((response) => response.json())
			.then((data) => {
				if (data.status) {
					Toastify({
						text: data.message,
						className: "toastify-success",
					}).showToast();
					setTimeout(function () {
						getData();
					}, 1000);
				} else {
					Toastify({
						text: data.message,
						className: "toastify-warning",
					}).showToast();
				}
			});
	} else {
		Toastify({
			text: "Cancellation successful !",
			className: "toastify-success",
		}).showToast();
	}
}
// Set password
const changePasswordForm = document.getElementById("formChangePassword");
if (changePasswordForm != null) {
	changePasswordForm.addEventListener("submit", function (event) {
		event.preventDefault(); 
		const username = document.getElementById("txt-username");
		const oldPassword = document.getElementById("txt-old-password");
		const newPassword = document.getElementById("txt-new-password");
		const reNewPassword = document.getElementById("txt-renew-password");
		if (username.value == "" || oldPassword.value == "" || newPassword.value == "" || reNewPassword.value == "") {
			Toastify({
				text: "Please provide the complete information !",
				className: "toastify-warning",
			}).showToast();
		} else if (oldPassword.value == newPassword.value) {
			Toastify({
				text: "The old password must not match the new password !",
				className: "toastify-warning",
			}).showToast();
		} else if (newPassword.value !== reNewPassword.value) {
			Toastify({
				text: "New pasword do not match !",
				className: "toastify-warning",
			}).showToast();
		} else {
			let userInfo = {
				username: username.value,
				oldPassword: oldPassword.value,
				newPassword: newPassword.value,
				reNewPassword: reNewPassword.value
			};
			let url = 'http://192.168.188.144:8080/user/change-password';
			fetch(url, {
				method: "POST",
				headers: {
					"Content-Type": "application/json; charset=utf-8",
				},
				body: JSON.stringify(userInfo),
			})
				.then((response) => response.json())
				.then((data) => {
					if (data.status) {
						Toastify({
							text: data.message,
							className: "toastify-success",
						}).showToast();
						setTimeout(() => {
							oldPassword.value = "";
							newPassword.value = "";
							reNewPassword.value = "";
						}, 500);
					} else {
						Toastify({
							text: data.message,
							className: "toastify-warning",
						}).showToast();
					}
				});
		}

	})
}

//
function showClientDetails() {
	const infoItems = document.querySelectorAll(".items-list .item-info");
	infoItems.forEach((item) => {
		item.addEventListener("click", () => {
			let infoItem = item.classList.contains("item-info-active");

			infoItems.forEach((el) => {
				el.classList.remove("item-info-active");
			});

			if (infoItem) item.classList.remove("item-info-active");
			else item.classList.add("item-info-active");
		});
	});
}

function renderItem(x, y) {
	let innerhtml = `<li class="item">
	    <div class="item-label label-${y[x]["server"]} label-server">
			<p>${y[x]["server"]}</p>
		</div>
		${y[x]["revoke"]
			? '<div class="item-label label-revoke"><p>revoked</p></div>'
			: ""
		}
		<div class="item-title">
			<div class="item-img">
				${y[x]["running_status"]["connected"] ? '' : "<i class='bx bx-wifi-off'></i>"}  
				<img class="camera" src="https://icons.iconarchive.com/icons/etherbrian/webuosities/32/live-cam-1-icon.png" width="32" height="32">
				<img class="phone" src="https://icons.iconarchive.com/icons/fasticon/hand-draw-iphone/32/iPhone-icon.png" width="32" height="32">
			</div>
			<span class="item-name">${x}</span>
		</div>
		<div class="btn-container"> 
			<p onclick="del('${y[x]["fullname"]}')">
				<i class='bx bx-x btn-delete' title="Delete client"></i> 
			</p> 
			<p onclick="revoke('${y[x]["fullname"]}')">
				<i class='bx bx-power-off btn-revoke' title="Revoke client"></i>
			</p> 
			<form >
				<input type="hidden" name="clientName[]" value="${x}">
				<p onclick="downLoadFileConfig('${y[x]["fullname"]
		}')" title="Download file" name="btnForm" class="btn-form"> 
					<i class='bx bx-download btn-download' ></i>
				</p>
			</form>
			<div class="item-info">
				<i class='bx bx-info-circle btn-info' title="View detail"></i> 
				<div class="item-details">
					<ul>
						<li>
							<span class="name-detail address">Address: </span>
							<span class="address-value">${y[x]["address"]}</span>
						</li>
						<li>
							<span class="name-detail privatekey">Real Address: </span>
							<p class="">${y[x]["running_status"]["real_address"]}</p>
						</li> 
						<li>
							<span class="name-detail privatekey">Bytes Sent: </span>
							<p class="">${parseInt(y[x]["running_status"]["bytes_sent"]).toLocaleString('en-US')}</p>
						</li> 
						<li>
							<span class="name-detail privatekey">Bytes Received: </span>
							<p class="">${parseInt(y[x]["running_status"]["bytes_received"]).toLocaleString('en-US')}</p>
						</li> 
						<li>
							<span class="name-detail privatekey">Connected Since: </span>
							<p class="">${y[x]["running_status"]["connected_since"]}</p>
						</li> 
					</ul>
					<span class="update-time">Updated: ${y[x]["update_time"]}</span>
				</div> 
			</div>
		</div> 
	</li>`;
	return innerhtml;
}

//
function renderTunelSelection(x = null, y = "No tunel running !!!") {
	return x == null
		? `<option value="">${y}</option>`
		: y == "All"
			? `<option value="${x}">${x}</option><option value="all" selected >All</option>`
			: `<option value="${x}">${x}</option>`;
}
function renderTunelInterface(x, clname = "power-off") {
	return x == null
		? `<p>No tunel !</p>`
		: `<div class="interface">
				<div>
					<div data-info="${x}" class="toggle toggle--push toggle--push--glow ${clname}" title="Click to up tunel">
						<input type="checkbox" class="toggle--checkbox" >
						<span class="toggle--btn" data-label-on="on"  data-label-off="off"></span>
					</div>
					<p class="interface-name">${x}</p>
				</div>
				<div>
					<a href="/tunel/${x}"><i class="bx bx-info-circle" title="View detail"></i></a>
				</div>
			</div>`;
}
// 
function renderTunelInterfaceDetail(x) {
	return `<div class="interface">
				<div>
					<div data-info="${x['name']}" class="toggle toggle--push toggle--push--glow ${x['status'] == 'active' ? 'power-active' : 'power-off'}" title="Click to up tunel">
						<input type="checkbox" class="toggle--checkbox" >
						<span class="toggle--btn" data-label-on="on"  data-label-off="off"></span>
					</div>
					<p class="interface-name">VPN services is ${x['status'] == 'active' ? 'running' : 'OFF'}</p>
				</div> 
			</div>
			<table class="table-tunel-detail">
				<tr>
					<td>Name:</td>
					<td>${x['name']}</td>
				</tr>
				<tr>
					<td>Tunel interface:</td>
					<td>${x['tunel_interface']}</td>
				</tr>
				<tr>
					<td>Tunel network:</td>
					<td>${x['tunel_network']}</td>
				</tr>
				<tr>
					<td>Current active Client:</td>
					<td>${x['current_active_client']}</td>
				</tr>
				<tr>
					<td>Port for VPN client connections:</td>
					<td>${x['tunel_protocol']}/${x['tunel_port']}</td>   
				</tr>
				<tr>
					<td>DNS server:</td>
					<td>${x['tunel_dns'][0]}, ${x['tunel_dns'][1]}</td>
				</tr>
			</table>`;
}
