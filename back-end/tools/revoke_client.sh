#!/bin/bash

CLIENT=$1
OP_CONFIG_PATH="/etc/openvpn"

cd "$OP_CONFIG_PATH"/easy-rsa/
./easyrsa --batch revoke "$CLIENT"
./easyrsa --batch --days=7300 gen-crl
rm -f "$OP_CONFIG_PATH"/server/crl.pem
cp "$OP_CONFIG_PATH"/easy-rsa/pki/crl.pem "$OP_CONFIG_PATH"/server/crl.pem

