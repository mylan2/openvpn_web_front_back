#!/bin/bash

# Define paths and variables
OP_CONFIG_PATH="/etc/openvpn"
SERVER_PUBLIC_IP="192.168.188.144"
SERVER_NAME=$2
CIPHER="AES-256-GCM"
# Test area
# IP_MAX=$1
# NET_MASK=$2
# Test area 
  
 
# Function to generate a new client configuration
generate_client_config() {
 
    CLIENT_NAME=$1 
    SERVER_PORT=$(cat /etc/openvpn/$SERVER_NAME.conf | grep port | cut -d ' ' -f 2) 
     # Create client configuration Staic IP
    CURRENT_SERVER=$(sudo ls /etc/openvpn | grep '^server.*.conf$' | cut -d '.' -f 1)
    # Browse each file and chose folder ccd with the same name
    for SERVER in "${CURRENT_SERVER[@]}"; do
        if [ "$SERVER" == "$SERVER_NAME" ]; then

            if ! [ -d "$OP_CONFIG_PATH/server/$SERVER_NAME.ccd" ]; then
                mkdir "$OP_CONFIG_PATH/server/$SERVER_NAME.ccd"
            fi
            if ! [ -e "$OP_CONFIG_PATH/server/$SERVER_NAME.ipp.txt" ]; then
                touch "$OP_CONFIG_PATH/server/$SERVER_NAME.ipp.txt"
            fi

            DIR_IP_ASSIGN="$OP_CONFIG_PATH/server/$SERVER_NAME.ccd"

            RANDOM_STRING=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 10 | head -n 1)

            CLIENT_NAME_FORMAT="$CLIENT_NAME"_"$SERVER_NAME"_"$RANDOM_STRING"
        fi
    done
    CLIENT_NAME=$CLIENT_NAME_FORMAT
    # craete client folder 
    mkdir $OP_CONFIG_PATH/client/$CLIENT_NAME
    # Generate client key pair
    cd $OP_CONFIG_PATH/easy-rsa/
    ./easyrsa --batch --days=7300 build-client-full $CLIENT_NAME nopass
    # copy client cert and client key to folder client
    cp $OP_CONFIG_PATH/easy-rsa/pki/issued/$CLIENT_NAME.crt $OP_CONFIG_PATH/client/$CLIENT_NAME/
    cp $OP_CONFIG_PATH/easy-rsa/pki/private/$CLIENT_NAME.key $OP_CONFIG_PATH/client/$CLIENT_NAME/
    # Create client config file ovpn
    touch $OP_CONFIG_PATH/client/$CLIENT_NAME/$CLIENT_NAME.ovpn
    
    echo "client 
dev tun
proto udp
remote $SERVER_PUBLIC_IP $SERVER_PORT
resolv-retry infinite
nobind
user nobody
group nogroup
persist-key
persist-tun
remote-cert-tls server
cipher $CIPHER
verb 3" > $OP_CONFIG_PATH/client/$CLIENT_NAME/$CLIENT_NAME.ovpn

 
    CLIENT_KEY_PATH=$OP_CONFIG_PATH/client/$CLIENT_NAME
    SERVER_KEY_PATH=$OP_CONFIG_PATH/server/
    OUTPUT_DIR=$OP_CONFIG_PATH/client/$CLIENT_NAME

    cat <(echo -e '<ca>') \
        ${SERVER_KEY_PATH}/ca.crt \
        <(echo -e '</ca>\n<cert>') \
        ${CLIENT_KEY_PATH}/$CLIENT_NAME.crt \
        <(echo -e '</cert>\n<key>') \
        ${CLIENT_KEY_PATH}/$CLIENT_NAME.key \
        <(echo -e '</key>\n<tls-crypt>') \
        ${SERVER_KEY_PATH}/ta.key \
        <(echo -e '</tls-crypt>') \
        >> $OP_CONFIG_PATH/client/$CLIENT_NAME/$CLIENT_NAME.ovpn 

    # Get tunel IP
    NET_WORK=$(cat $OP_CONFIG_PATH/$SERVER_NAME.conf | grep -m 1 -w ifconfig | cut -d' ' -f 2) 
    NET_MASK=$(cat $OP_CONFIG_PATH/$SERVER_NAME.conf | grep -m 1 -w ifconfig | cut -d' ' -f 3)
    START_IP="10" 
    # Get Client largest IP address
    # Check folder is existing
    if [ -d "$DIR_IP_ASSIGN" ]; then
        # Get file list in folder
        # Count amount of files in folder
        FILE_COUNT=$(ls -l "$DIR_IP_ASSIGN" | grep ^- | wc -l)
        # Check if folder is empty
        if [ $FILE_COUNT -eq 0 ]; then  
            # Get NET ID
            NET_ID=$(cut -d'.' -f 1,2,3 <<< $NET_WORK)
            # Set default ip address starting 
            IP_MAX="$NET_ID.$START_IP"
        else
            # If folder is not empty
            # Get file list in folder
            FILES=("$DIR_IP_ASSIGN"/*)
            # Browse each file and save into array
            for FILE in "${FILES[@]}"; do
                if [ -f "$FILE" ]; then
                    # Save file contents to array
                    IPS=("${IPS[@]}" "$(cat "$FILE" | cut -d ' ' -f 2  )")
                fi
            done
            # Get largest IP address
            IP_MAX=($(for ip in "${IPS[@]}"; do echo $ip; done | sort -r))
            IP_MAX=${IP_MAX[0]}
        fi
    else
        echo "DIR_IP_ASSIGN not found."
    fi
    
    # Declare components of IP 
    OCTET_4=$(cut -d'.' -f 4 <<< $IP_MAX)
    OCTET_3=$(cut -d'.' -f 3 <<< $IP_MAX)
    OCTET_2=$(cut -d'.' -f 2 <<< $IP_MAX)  
    # Generate DYNAMIC IPv4
    case $NET_MASK in
        "255.255.255.0")
            NET_ID=$(cut -d'.' -f 1,2,3 <<< $NET_WORK)
            if [ $(($OCTET_4)) -lt 254 ]
                then
                    IP_NEXT=$(($OCTET_4 + 1))
                    CLIENT_ADDRESS=$NET_ID.$IP_NEXT 
            fi
            # Test
            # echo $CLIENT_ADDRESS
            ;;
        "255.255.0.0")
            NET_ID=$(cut -d'.' -f 1,2 <<< $NET_WORK)
            if [ $OCTET_4 -lt 254 ]
            then 
                IP_NEXT=$(($OCTET_4 + 1))
                CLIENT_ADDRESS=$NET_ID.$OCTET_3.$IP_NEXT 
            else  
                if [ $(($OCTET_3)) -lt 254 ]
                then 
                    IP_NEXT=$(($OCTET_3 + 1))
                    CLIENT_ADDRESS=$NET_ID.$IP_NEXT.$START_IP  
                fi
            fi
            # Test
            # echo $CLIENT_ADDRESS
            ;;
        *)
            NET_ID=$(cut -d'.' -f 1 <<< $NET_WORK)
            if [ $OCTET_4 -lt 254 ]
            then 
                IP_NEXT=$(($OCTET_4 + 1))
                CLIENT_ADDRESS=$NET_ID.$OCTET_2.$OCTET_3.$IP_NEXT  
            else
                if [ $(($OCTET_3)) -lt 254 ]
                then 
                    IP_NEXT=$(($OCTET_3 + 1))
                    CLIENT_ADDRESS=$NET_ID.$OCTET_2.$IP_NEXT.$START_IP
                    
                else
                    if [ $(($OCTET_2)) -lt 254 ]
                    then 
                        IP_NEXT=$(($OCTET_2 + 1))
                        if [ $OCTET_3 -gt 253 ]
                        then
                            OCTET_3="0"
                        fi
                        CLIENT_ADDRESS=$NET_ID.$IP_NEXT.$OCTET_3.$START_IP 
                    fi
                fi
            fi
            # Test
            # echo $CLIENT_ADDRESS
            ;;
    esac
    # Assign ip addresses to client
    touch $DIR_IP_ASSIGN/$CLIENT_NAME
    echo "ifconfig-push $CLIENT_ADDRESS $NET_MASK" > "$DIR_IP_ASSIGN/$CLIENT_NAME"

}

# Function to initialize WireGuard server
initialize_openvpn() {
    if ! [ -d "$OP_CONFIG_PATH/easy-rsa" ]; then
        make-cadir $OP_CONFIG_PATH/easy-rsa  
    fi
    # 
    if ! [ -e "$OP_CONFIG_PATH/server/server.crt" ]; then
        cd "$OP_CONFIG_PATH/easy-rsa" 
        ./easyrsa --batch init-pki
        ./easyrsa --batch build-ca nopass
        # Server Keys and Certificates
        # generate a key pair for the server:
        # easyrsa --passin=pass:<CA-PASS> --passout=pass:<CERT-PASS> build-server-full server
        ./easyrsa --batch --days=7300 build-server-full server nopass
        ./easyrsa --batch --days=7300 gen-crl 
        openvpn --genkey secret ta.key 
        # copy these files to the server folder
        cp pki/dh.pem pki/ca.crt ta.key pki/crl.pem pki/issued/server.crt pki/private/server.key /etc/openvpn/server
    fi
   
    #
    cd "$OP_CONFIG_PATH/"
    touch $SERVER_NAME.conf
    if ! [ -d "$OP_CONFIG_PATH/server/$SERVER_NAME.ccd" ]; then
        mkdir "$OP_CONFIG_PATH/server/$SERVER_NAME.ccd"
    fi
    
    echo "mode server
tls-server
management "$SERVER_PUBLIC_IP" 8989
local "$SERVER_PUBLIC_IP"
port 1194
proto udp
dev tun
ca server/ca.crt
cert server/server.crt
key server/server.key
dh server/dh.pem
crl-verify server/crl.pem
topology subnet
push 'topology subnet'
ifconfig 10.8.0.1 255.255.255.0
push 'route-gateway 10.8.0.1'
ifconfig-pool 10.8.0.10 10.8.0.254 255.255.255.0
ifconfig-pool-persist server/$SERVER_NAME.ipp.txt
client-config-dir server/$SERVER_NAME.ccd 
push 'dhcp-option DNS 8.8.8.8'
push 'dhcp-option DNS 1.1.1.1'
client-to-client
keepalive 10 120
tls-crypt server/ta.key
cipher AES-256-GCM
auth SHA256
user nobody
group nogroup
persist-key
persist-tun
status /var/log/openvpn/openvpn-$SERVER_NAME-status.log
log /var/log/openvpn/openvpn-$SERVER_NAME.log
log-append  /var/log/openvpn/openvpn-$SERVER_NAME.log
verb 3
explicit-exit-notify 1" > $SERVER_NAME.conf
    
    sed -i "s/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g" /etc/sysctl.conf
    sysctl -p /etc/sysctl.conf
    systemctl restart openvpn@$SERVER_NAME

}

# Main script

# Check if OpenVPN is installed
if ! systemctl is-active --quiet openvpn; then
    if ! [ -d "$OP_CONFIG_PATH" ]; then
        echo "May be openvpn is not installed !"
    fi
fi

# Check if running as root
if [ "$EUID" -ne 0 ]; then
    echo "Please run this script as root or with sudo !"
    exit 1
fi
# Check if OpenVPN is already initialized
if ! [ -e "$OP_CONFIG_PATH/$SERVER_NAME.conf" ]; then
    initialize_openvpn
fi


# Allow adding clients
NAME=$1
generate_client_config "$NAME"


 
