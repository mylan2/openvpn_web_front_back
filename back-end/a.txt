client
dev tun
proto udp
remote a.txt
resolv-retry infinite
nobind
user nobody
group nogroup
persist-key
persist-tun
remote-cert-tls server
cipher 
verb 3
<ca>
-----BEGIN CERTIFICATE-----
MIIDQjCCAiqgAwIBAgIUSUxQo7Jtmd4GxNKVK0BuMcIDabQwDQYJKoZIhvcNAQEL
BQAwEzERMA8GA1UEAwwIQ2hhbmdlTWUwHhcNMjQwMTI5MDEzMzA3WhcNMzQwMTI2
MDEzMzA3WjATMREwDwYDVQQDDAhDaGFuZ2VNZTCCASIwDQYJKoZIhvcNAQEBBQAD
ggEPADCCAQoCggEBAM61EMMIR8wJZWUICX30qOnQIsP4OLDEoGr0AFn6LTC0ZEL0
7VSg3EQ01UWQ/Lfjg5ufVe774xCtI/Bz1wJHVSQvOdg0J3wZFW62KN3WX02HJGMI
dpMg+ZJmgZor0m50rjWBGv2CpPY5mp9ll8vAdKXMxqDYHLeE0DnVCsCAt/e04tlt
vZasoLPK9IkD2YdVrSIExNuFQyCZrhoBTcFL4ewowAQ/F+r3cqPUlMSlWr5QfiGN
tTk7TuHfqmTaxGY5G/DaH+kolXvfzt2EFzXScnV8oJMTUYfSvXFG7UsSaFMnO1SF
KKRVrSPhzF+4E3EeCOrJb1RBS5SqGksA3umzbn0CAwEAAaOBjTCBijAdBgNVHQ4E
FgQUdGvmpqyJZbNkuyWG6LEQEHPSDAgwTgYDVR0jBEcwRYAUdGvmpqyJZbNkuyWG
6LEQEHPSDAihF6QVMBMxETAPBgNVBAMMCENoYW5nZU1lghRJTFCjsm2Z3gbE0pUr
QG4xwgNptDAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG9w0BAQsF
AAOCAQEAL53uJNRMYiHBrUzrEKX0Vzwil9MqXilL7rnfKknkqaLpqP1/Yn2XmWq8
BxQ1PntY4CpzX9iCdspejirgWG+gWB2EnBZfptcQGrRBLtEpkSFKjpfgFfxc133c
GNqlBD80aYbCvmfpojwN48TMXRhS8XGtkwH03LSRnTlL82hwV+orgRthhvLsdtie
4VXKh0TIwCgJSuxIsXsrQDf19N3kQ9+GAyuVYcjqe5FNDmsEoXdv1mR+y43kwr15
wrwQKgiYq4IolQcHrQcG8G3dv+gE6sBHK5TUP+7Gtwc8O6hR49nxVsPvdZwejGxY
RU7eC9EIQYIXyNc/65AOcJzahGnmvA==
-----END CERTIFICATE-----

</ca>
<cert>
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            87:0f:c9:29:be:c6:1f:b6:36:c7:68:c7:44:33:71:b5
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: CN=ChangeMe
        Validity
            Not Before: Feb 23 07:07:21 2024 GMT
            Not After : Feb 18 07:07:21 2044 GMT
        Subject: CN=client28_server01_MnFLuTTGmu
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:d8:51:79:df:0c:1e:d7:bd:2f:f3:b5:d7:a7:2d:
                    f4:fc:53:a7:45:24:ea:a5:78:cd:bd:6a:5c:0b:19:
                    87:0f:5c:67:4f:14:1e:a4:44:6f:f7:79:ad:43:c2:
                    b5:30:77:4d:ad:c4:cd:63:cf:ba:a0:5a:0e:78:8d:
                    76:87:0e:6e:58:34:00:7e:02:82:f1:10:7c:04:83:
                    b9:38:2b:3d:7d:1d:85:98:5e:8b:de:8b:15:2a:eb:
                    37:ea:5b:60:bb:74:d1:76:97:1b:b2:71:c9:03:bc:
                    cb:7a:f3:51:67:42:b1:ca:3c:33:31:c7:8c:b2:c6:
                    3f:22:41:39:eb:63:39:26:16:f2:9b:f2:46:0a:c5:
                    61:aa:88:f8:71:be:62:1f:d3:5b:8a:ad:f3:70:47:
                    87:f9:3e:5d:d6:0d:aa:24:99:1b:5a:80:cd:e6:e7:
                    64:7a:a6:d3:53:d7:b5:05:b0:5d:d2:fd:bd:2e:4d:
                    90:3f:cd:34:77:f0:6a:6e:87:33:cc:0c:5c:09:84:
                    de:1b:be:de:83:08:1d:5d:96:3d:a7:46:65:08:a4:
                    6f:16:7a:f4:f4:a4:cd:ed:1a:46:b7:ac:3c:64:8b:
                    c2:01:a8:7a:c1:a7:ee:4c:2c:6d:ca:0e:30:9b:cd:
                    f8:77:99:c5:a4:99:5d:14:f3:6e:7f:3b:f5:ce:9e:
                    d7:09
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            X509v3 Subject Key Identifier: 
                82:FA:40:CB:0A:DD:F4:C6:2B:C0:37:E0:28:06:30:C2:AC:D9:BB:07
            X509v3 Authority Key Identifier: 
                keyid:74:6B:E6:A6:AC:89:65:B3:64:BB:25:86:E8:B1:10:10:73:D2:0C:08
                DirName:/CN=ChangeMe
                serial:49:4C:50:A3:B2:6D:99:DE:06:C4:D2:95:2B:40:6E:31:C2:03:69:B4
            X509v3 Extended Key Usage: 
                TLS Web Client Authentication
            X509v3 Key Usage: 
                Digital Signature
    Signature Algorithm: sha256WithRSAEncryption
    Signature Value:
        95:63:2d:91:cb:32:56:3e:30:f2:a5:dc:f1:bf:71:3a:c0:4b:
        3c:dd:66:20:b2:33:39:e8:c8:40:d5:f6:fb:fb:28:87:61:07:
        5d:af:ab:27:2d:c0:51:b9:5a:a6:be:e0:8e:a5:63:ef:f3:28:
        45:32:a4:65:3c:3b:61:1e:4c:c2:0b:32:ac:c7:38:f5:c5:16:
        93:f4:99:f5:41:f8:14:e5:00:84:bb:c5:66:02:a1:3e:93:38:
        c1:c9:62:03:fc:61:28:aa:47:f6:94:3c:69:91:2b:e5:a3:b4:
        d9:26:1a:5b:8b:c5:6d:65:0b:f9:9f:cf:49:96:9a:fa:ab:4a:
        ea:e9:59:04:b7:bf:e7:48:17:8b:45:fc:eb:8d:f6:bf:7b:c2:
        be:f7:78:da:3d:fe:83:0e:7a:df:68:da:60:a1:17:45:a2:94:
        f7:06:9d:33:2d:12:66:dc:5b:34:77:2a:2a:4e:b5:01:16:be:
        57:11:57:e4:da:64:41:09:7c:d9:ff:d9:48:24:27:6f:89:ec:
        77:dd:dd:9c:16:b3:a0:be:da:8e:04:9f:56:65:b2:34:f5:26:
        de:47:0f:7f:a7:a4:0e:7c:1f:88:a7:55:54:1d:16:11:32:66:
        ed:93:06:59:ec:2a:1c:b5:08:a3:55:56:4f:67:c4:80:d0:c8:
        b6:b8:01:67
-----BEGIN CERTIFICATE-----
MIIDZTCCAk2gAwIBAgIRAIcPySm+xh+2Nsdox0QzcbUwDQYJKoZIhvcNAQELBQAw
EzERMA8GA1UEAwwIQ2hhbmdlTWUwHhcNMjQwMjIzMDcwNzIxWhcNNDQwMjE4MDcw
NzIxWjAnMSUwIwYDVQQDDBxjbGllbnQyOF9zZXJ2ZXIwMV9NbkZMdVRUR211MIIB
IjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2FF53wwe170v87XXpy30/FOn
RSTqpXjNvWpcCxmHD1xnTxQepERv93mtQ8K1MHdNrcTNY8+6oFoOeI12hw5uWDQA
fgKC8RB8BIO5OCs9fR2FmF6L3osVKus36ltgu3TRdpcbsnHJA7zLevNRZ0Kxyjwz
MceMssY/IkE562M5Jhbym/JGCsVhqoj4cb5iH9Nbiq3zcEeH+T5d1g2qJJkbWoDN
5udkeqbTU9e1BbBd0v29Lk2QP800d/BqboczzAxcCYTeG77egwgdXZY9p0ZlCKRv
Fnr09KTN7RpGt6w8ZIvCAah6wafuTCxtyg4wm834d5nFpJldFPNufzv1zp7XCQID
AQABo4GfMIGcMAkGA1UdEwQCMAAwHQYDVR0OBBYEFIL6QMsK3fTGK8A34CgGMMKs
2bsHME4GA1UdIwRHMEWAFHRr5qasiWWzZLslhuixEBBz0gwIoRekFTATMREwDwYD
VQQDDAhDaGFuZ2VNZYIUSUxQo7Jtmd4GxNKVK0BuMcIDabQwEwYDVR0lBAwwCgYI
KwYBBQUHAwIwCwYDVR0PBAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQCVYy2RyzJW
PjDypdzxv3E6wEs83WYgsjM56MhA1fb7+yiHYQddr6snLcBRuVqmvuCOpWPv8yhF
MqRlPDthHkzCCzKsxzj1xRaT9Jn1QfgU5QCEu8VmAqE+kzjByWID/GEoqkf2lDxp
kSvlo7TZJhpbi8VtZQv5n89Jlpr6q0rq6VkEt7/nSBeLRfzrjfa/e8K+93jaPf6D
DnrfaNpgoRdFopT3Bp0zLRJm3Fs0dyoqTrUBFr5XEVfk2mRBCXzZ/9lIJCdviex3
3d2cFrOgvtqOBJ9WZbI09SbeRw9/p6QOfB+Ip1VUHRYRMmbtkwZZ7CoctQijVVZP
Z8SA0Mi2uAFn
-----END CERTIFICATE-----

</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDYUXnfDB7XvS/z
tdenLfT8U6dFJOqleM29alwLGYcPXGdPFB6kRG/3ea1DwrUwd02txM1jz7qgWg54
jXaHDm5YNAB+AoLxEHwEg7k4Kz19HYWYXoveixUq6zfqW2C7dNF2lxuycckDvMt6
81FnQrHKPDMxx4yyxj8iQTnrYzkmFvKb8kYKxWGqiPhxvmIf01uKrfNwR4f5Pl3W
DaokmRtagM3m52R6ptNT17UFsF3S/b0uTZA/zTR38GpuhzPMDFwJhN4bvt6DCB1d
lj2nRmUIpG8WevT0pM3tGka3rDxki8IBqHrBp+5MLG3KDjCbzfh3mcWkmV0U825/
O/XOntcJAgMBAAECggEAFjcj33rErh5RTuOVbqdjGgjTTGASswS5iGLkagVon/S6
7V2hGfinURCPcT6NUhlmglqMR1xdi7tD8jCBBDTAs1E/3bEPVod9XQ9UFccnDPLb
tf67uZKqDVrTxr80q3u/SUESiJxz6uzYLtvPN94bGSxei5W1RuTiNffh5WEuzNDt
AEsqfeLBeLr4FOlookuclnEubSqKWJYXL5PYJihc8rxKAfG5QtkeS94ejn81DVIJ
7iOOSY9czI+MYp8ecRvVGJaTIFIG6zz0ayKHluNNxijyosBuM88fDIPYUJWqtUzg
3Yu2pPg9Luwn3azCjZcpX3ZbpIpWpnRYnYlum4QmKQKBgQDYoh2usKYc5KfTgEmR
IZaWsoO/6qOl8mKYXHpXIFjOQxMFl2DUbgLE9IHNKVQpDoQNPN2rpPqX6maipThm
+QWUye898V4dlmiMZUnFOqon3WoF/PweAZ9Inqwi7aLckp7yBg5l/BVo/TAU70UE
IfguOxOUXFmHmkE60Afd/qcpzQKBgQD/oLTITVoKFZEWCaW/aa3fcsoIYE8tPN8q
TSrja2KCpGZHuMMNA2cZJ/VpaASDQNlWeeQdPFYH1fbUxOU/kdd5Fnr0UuFX9FSS
nv4brVURWl1340+Z1e5OJjYemZlFidzoO8jIKSKwjJJjKzUvz0sc4i4sdJ5r9PF9
pkNlndh2LQKBgBRCbQY2TQKsu6lC+R5ShtPHFB0xo2ZUmbXoCCT3yHZZvYCrXInh
U+75ct4t/EnuqpjWVWDFCA6HBFw3mWO/7C6zB+XCGewocdSXcGO6jck2osC2nRlt
S2kuNaj6kl2QdL7rpBMTlSkBbKq4ibrONniUW8tnK5RL78fBRssTEq0NAoGBAJ1W
uy82fR8L2ztnQL+qOjz4mGXDcbh0aR/OU4GbDQ1ARzwJ+vQ9je2r7J+uSGjJN3zE
laVxB7rW9ZBMiJYyHCAJuDA9+zl5x7xfZCSA8YU2y6UJImubg4xSdKks/WwRyQig
E5svxoxIWan0ZE5DFwZ2KNlwBjqIj0M5XO5k02EZAoGASjVYrOyQ9sLfRIbvNLkt
dUFcK9jI4iqcdyo7qN7/bHdm0qwxq3NBKTm6XNHpYy8U5ATSAMtb3A37/NummoOK
PDxei7kn6zOsHB+LRUkO0i7nNqoAGzidwCKlnuOse4Ch5vDeF26K5E9mQKM+FmO+
UqkbU5+oVH4ehPFDL2Wa5xY=
-----END PRIVATE KEY-----

</key>
<tls-crypt>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
dd2966b477e294f30f5252f6a70e6ce4
e6f85abae8a00644a9a9b616a9096e49
1b2c6543cbd0438b2d91d149f1bef3f2
54a6159c1f18a7de0f4048f8fcbe011c
17a68cf93ecc2960a33dc1206087ac80
5603abe36528cc934a5e5303dc0df6dd
02fe9d2a9d4bfba8af54d7eb8e2136fc
c1fdbcf03bc20720cc1030f5cf0a0e33
66ce546501d4d96fda3a6645568d7b40
52c536bc37612091d55fa9fd8dadbfda
5c1126acf50054476f3044b21e4f9234
30c6bbd895b2e695a3b78fadbec88da7
f5a5ad2d2daa24c7128db5f0b36249eb
4369c1518e8a4d4459a8779abf18ba4d
a770c28e25907b7ca286fd51d5a06698
80d2e8ac7eb5f88f3235bd466bbd27f3
-----END OpenVPN Static key V1-----

</tls-crypt>

