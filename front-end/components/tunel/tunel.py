from flask import Blueprint, render_template, request, session
 

tunel_dp = Blueprint('tunel', __name__, template_folder="templates")


@tunel_dp.route('/tunel/<name>', methods=['GET', 'POST'])
def tunel_detail(name):
    if 'user' not in session:
        return render_template('error.html') 
    return render_template('tunel-detail.html')
##  =========================================== API AREA ========================================================
