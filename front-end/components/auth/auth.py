from flask import Blueprint, render_template, request, session
import hashlib, json

from helpers.init_helpers import connection_db

auth_dp = Blueprint("auth",
                     __name__,
                     template_folder="templates",
                     static_folder='static',
                     static_url_path='/components/auth')

# Login and logout
@auth_dp.route('/login', methods=['GET', 'POST'])
def login():
    if 'user' in session:
        session.pop('user', None)
    
    return render_template('login.html')

# API Login
@auth_dp.route('/auth/login', methods=['POST'])
def handle_login():
    if request.method == 'POST':
        data = request.get_json()
        user_name = data['username']
        pass_word = data['password'] 
        if len(user_name) < 1 or len(pass_word) < 1:
            return json.dumps({
                'status' : False,
                'message' : 'The name or password is not empty !'
            })
        else: 
            # Connect database 
            if connection_db() is False:
                return json.dumps({
                    'status' : False,
                    'message' : 'Database connection failed!'
                })
            # 
            cur, conn = connection_db()
            # 
            # pass_word = hashlib.md5(pass_word.encode('utf-8')).hexdigest()
            select_user_cmd = "SELECT username, password FROM users WHERE username = %s and password = %s"
            user_info = (user_name , pass_word)
            try:
                cur.execute(select_user_cmd, user_info) 
                result_user_excute = cur.fetchall()
                if len(result_user_excute) > 0:
                    user = {
                        'username': user_name,
                        'password': pass_word
                    }
                    session['user'] = user 
                    return json.dumps({
                        'status' : True,
                        'message' : 'Login Successfully !'
                    })    
                else:
                    return json.dumps({
                        'status' : False,
                        'message' : 'Login Failed !'
                    }) 
            except Exception as e:
                return json.dumps({
                        'status' : False,
                        'message' : str(e)
                    })  
            finally:
            # Close cursor and database connection
                cur.close()