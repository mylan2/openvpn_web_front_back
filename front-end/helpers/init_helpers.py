import mysql.connector, os
from dotenv import load_dotenv
load_dotenv()

# Connect to the database
def connection_db(): 
    # Variables
    db_url = os.getenv("DB_URL")
    db_port = os.getenv("DB_PORT")
    db_name = os.getenv("DB_NAME")
    db_user = os.getenv("DB_USER")
    db_password = os.getenv("DB_PASSWORD")
    try:
        conn = mysql.connector.connect(
                host = db_url,
                port = str(db_port),
                user = db_user,
                password = db_password,
                database = db_name
            )    
        cur = conn.cursor()
        return cur, conn 
    except mysql.connector.Error as err:
        print("Database connection failed:", err)
        return False


