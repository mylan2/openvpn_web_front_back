from flask import Blueprint, render_template, session

client_dp = Blueprint('client', 
                     __name__,
                     template_folder="templates")

# Index
@client_dp.route('/clients')
def index():
    if 'user' not in session:
        return render_template('error.html')
    
    return render_template('clients.html')
