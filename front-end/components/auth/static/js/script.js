
// Login
const b = document;
const loginForm = b.getElementById("loginForm");

loginForm.addEventListener("submit", (event) => {
    event.preventDefault();
    const formData = new FormData(loginForm);
    let username =  formData.get('username').trim();
    let password =  CryptoJS.MD5(formData.get('password').trim()).toString();
    if (username.length == 0 || password.length == 0) {
        Toastify({
            text: 'The name or password is not empty !',
            className: 'toastify-warning',
        }).showToast();
        return;
    }
    const userInfo = {
        username,
        password
    }

    fetch("/auth/login", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(userInfo)
    })
    .then((response) => {
        if (!response.ok) {
            Toastify({
                text: 'Some thing went wrong with Network !',
                className: 'toastify-warning',
            }).showToast(); 
        }
        return response.json(); 
    })
    .then((data) => {
        if (data.status) {
            Toastify({
                text: data.message,
                className: "toastify-success",
            }).showToast();
            setTimeout(() => {
                location.href = '/clients'
            }, 2000);

        } else {
            Toastify({
                text: data.message,
                className: "toastify-warning",
            }).showToast();
        }
    }).catch(error => { 
        Toastify({
            text: 'There was a problem with the login: ' + error,
            className: 'toastify-warning',
        }).showToast(); 
        console.error('There was a problem with the login:', error);
    });
}); 