import os
from dotenv import load_dotenv
from flask import Flask
from datetime import timedelta 

from components.auth.auth import auth_dp
from components.client.client import client_dp
from components.tunel.tunel import tunel_dp
from components.user.user import user_dp 

load_dotenv()
app_secret_key = os.getenv("APP_SECRET_KEY")

app = Flask(__name__)
app.config['SECRET_KEY'] = app_secret_key
app.permanent_session_lifetime = timedelta(minutes=100)

app.register_blueprint(auth_dp)
app.register_blueprint(client_dp)
app.register_blueprint(tunel_dp)
app.register_blueprint(user_dp)

# 
if __name__ == "__main__":
    app.run(host="192.168.188.144", port="8081", debug=True)