from flask import Blueprint, request
import json
from helpers.init_helpers import connection_db, get_secret, get_tunel, get_tunel_status, run_cmd
 

tunel_dp = Blueprint('tunel', __name__)
sudo_permission = get_secret()
 
##  =========================================== API AREA ========================================================

@tunel_dp.route('/tunel/get-all-tunel', methods=['GET'])
def handle_get_tunel():
    if request.method == "GET":
        data = get_tunel()
        status_code = data['statusCode']
        tunels = data['tunels']
        tunels_error = data['error']
        if status_code != 0 or not tunels : 
            return json.dumps({
            'status' : False,
            'tunels' : [],
            'message' : tunels_error
            })
        # Save tunels into Database
        # Connect database
        cur, conn = connection_db()
        #   
        try:
            # Save server keys into Database
            cur.execute('SELECT * FROM server_keys WHERE 1;') 
            server_keys_excute_rows = cur.fetchall()
            if not server_keys_excute_rows:
                insert_server_keys = "INSERT INTO server_keys (server_key_id, server_key, server_cert, server_dh, server_tls, server_crl, server_ca) VALUES (%s, %s, %s, %s, %s, %s, %s);" 
                try: 
                    server_key = run_cmd(f"{sudo_permission} cat /etc/openvpn/server/server.key").stdout.strip()
                    server_ca = run_cmd(f"{sudo_permission} cat /etc/openvpn/server/ca.crt").stdout.strip()
                    server_cert = run_cmd(f"{sudo_permission} cat /etc/openvpn/server/server.crt").stdout.strip()
                    server_tls = run_cmd(f"{sudo_permission} cat /etc/openvpn/server/ta.key").stdout.strip()
                    server_crt = run_cmd(f"{sudo_permission} cat /etc/openvpn/server/crl.pem").stdout.strip()
                    server_dh = run_cmd(f"{sudo_permission} cat /etc/openvpn/server/dh.pem").stdout.strip()
                     
                    cur.execute(insert_server_keys, (None, server_key, server_cert, server_dh, server_tls, server_crt, server_ca))
                    conn.commit()

                except Exception as e:
                    conn.rollback() 
                    print('1/ ' + str(e))
            # 
            cur.execute('SELECT server_id, server_name FROM servers WHERE 1;') 
            server_excute_rows = cur.fetchall() 
            insert_query = "INSERT INTO servers (server_id, server_name, server_config) VALUES (%s, %s, %s);"  
            
            if len(server_excute_rows) > 0: 
                tunel_excute_rows_format = [] 
                for i in server_excute_rows:
                    tunel_excute_rows_format.append((i[1]))

                unique_values = set(tunel_excute_rows_format) ^ set(tunels)
                unique_values = list(unique_values)
                
                if(len(unique_values)) > 0:  
                    for i in unique_values:
                        try: 
                            server_config = run_cmd(f"cat /etc/openvpn/{i}.conf")
                            server_config_result = server_config.stdout.strip()
                            cur.execute(insert_query, (None, i, server_config_result))
                            conn.commit()

                        except Exception as e: 
                            print('2/ ' + str(e))
                        finally:
                            # Close cursor and database connection
                            cur.close() 
            else: 
                try:
                    for i in tunels:
                        server_config = run_cmd(f"cat /etc/openvpn/{i}.conf")
                        server_config_result = server_config.stdout.strip()
                        cur.execute(insert_query, (None, i, server_config_result))
                        conn.commit()
                    
                except Exception as e: 
                    conn.rollback()
                    print('3/ ' + str(e))
                finally:
                    # Close cursor and database connection
                    cur.close()
        except Exception as e:
            print('4/ ' + str(e))
        finally:
        # Close cursor and database connection
            cur.close()
        # 
        obj_tunel = {}
        obj_tunel['servers'] = tunels
        obj_status ={}
        for tunel in tunels: 
            status = get_tunel_status(tunel)
            obj_temp = {}
            obj_temp['status'] = status        
            obj_status[tunel] = obj_temp
        obj_tunel['status'] = obj_status
        return json.dumps({
            'status' : True,
            'tunels' : obj_tunel
        })
# API Get tunel Detail
@tunel_dp.route('/tunel/<tunel>/detail', methods=['GET'])
def handle_get_tunel_detail(tunel):  
    # if 'user' not in session:
    #     return json.dumps({
    #      'status' : False,
    #       'message' : 'You do not have permission to access this API !'
    #     }) 

    if request.method == 'GET':
        # data = request.get_json()
        # server_name = data['server_name']
        server_name = tunel
        server_config = server_name + '.conf'

        port_cmd = f"cat /etc/openvpn/{server_config} | grep port | cut -d ' ' -f 2"
        protocol_cmd = f"cat /etc/openvpn/{server_config} | grep proto | cut -d ' ' -f 2" 
        dns_cmd = f'cat /etc/openvpn/{server_config} | grep DNS | cut -d " " -f 4 | cut -d "\'" -f 1'
        network_cmd = f"cat /etc/openvpn/{server_config} | grep -m 1 -w ifconfig | cut -d' ' -f 2-3" 

        run_port_cmd = run_cmd(port_cmd)
        run_protocol_cmd = run_cmd(protocol_cmd)
        run_dns_cmd = run_cmd(dns_cmd)
        run_network_cmd = run_cmd(network_cmd)

        tunel_port = run_port_cmd.stdout.strip()
        tunel_protocol = run_protocol_cmd.stdout.strip()
        tunel_dns = run_dns_cmd.stdout.strip().split('\n') 
        server_address = run_network_cmd.stdout.strip()
        server_netmask = server_address.split(' ')[1]
        ip_address = server_address.split(' ')[0]

        if server_netmask == '255.255.255.0':
            prefix_length = '/24'
            max_clients = int(254 - 10)
            netID = server_address.split('.')[0] + '.' +  server_address.split('.')[1] + '.' +  server_address.split('.')[2]
        if server_netmask == '255.255.0.0':
            prefix_length = '/16'
            max_clients = int(65535 -10)
            netID = server_address.split('.')[0] + '.' +  server_address.split('.')[1] 
        if server_netmask == '255.0.0.0':
            prefix_length = '/8'
            max_clients = int(16777215 - 10)
            netID = server_address.split('.')[0]

        interface_cmd = f"ip a | grep '{ip_address}{prefix_length} scope global tun' |  awk -F' ' '{{print $5}}'"
        client_active_cmd = f"{sudo_permission} cat /var/log/openvpn/openvpn-{server_name}-status.log | grep -c {netID}"
        
        run_interface_cmd = run_cmd(interface_cmd)
        run_active_client_cmd = run_cmd(client_active_cmd)

        current_active_client = run_active_client_cmd.stdout.strip()
        tunel_interface = 'Not running' if not run_interface_cmd.stdout.strip() else run_interface_cmd.stdout.strip()
        status = get_tunel_status(server_name)
        
        tunel_config = {
            "name": server_name,
            "tunel_port": tunel_port,
            "tunel_protocol": tunel_protocol,
            "tunel_dns": tunel_dns,
            "tunel_network": ip_address+prefix_length,
            "tunel_interface": tunel_interface,
            "max_clients": max_clients,
            "status": status,
            "current_active_client": current_active_client
        }
        return json.dumps(
        {
            'status' : True,
            'tunelConfig' : tunel_config
        })
    return json.dumps(
        {
            'status' : False,
            'tunelConfig' : {}
        })
 
# 
@tunel_dp.route('/tunel/<tunel>/up', methods=['POST'])
def handle_up_tunel(tunel):
    if request.method == 'POST':
        # data = request.get_json()
        # tunel = data['tunel']
        
        if tunel is None:
            return json.dumps({
              'status' : False,
              'message' : 'The tunel is not exist !'
            })
        # 
        status = get_tunel_status(tunel) 
        if status == 'active':
            return json.dumps({
             'status' : False,
             'message' : 'The tunel is already running !'
            })
        # 
        run_start_tunel_cmd = run_cmd(f"{sudo_permission} systemctl start openvpn@{tunel}")
        status_code = run_start_tunel_cmd.returncode 
        start_tunel_err = run_start_tunel_cmd.stderr

        if status_code != 0: 
            return json.dumps({
            'status' : False,
            'message' : start_tunel_err
            })   
        return json.dumps({
            'status' : True,
            'message' : "Tunel is started successfully !"
            })