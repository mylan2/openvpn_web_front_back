import json, mysql.connector, os
from subprocess import run 
from dotenv import load_dotenv 
from cryptography.fernet import Fernet
from dotenv import load_dotenv
 
load_dotenv()

# Connect to the database
def connection_db(): 
    # Variables
    db_url = os.getenv("DB_URL")
    db_port = os.getenv("DB_PORT")
    db_name = os.getenv("DB_NAME")
    db_user = os.getenv("DB_USER")
    db_password = os.getenv("DB_PASSWORD")
    try:
        conn = mysql.connector.connect(
                host = db_url,
                port = str(db_port),
                user = db_user,
                password = db_password,
                database = db_name
            )   
        cur = conn.cursor()
        return cur, conn 
    except mysql.connector.Error as err:
        print("Database connection failed:", err)
        return False

# 
def get_secret(): 
    # Variables
    fixed_key = os.getenv("FIXED_KEY").encode()
    api_key = os.getenv("SECRET") 
    # Functions
    def encrypt_string(data):
        cipher_suite = Fernet(fixed_key)
        encrypted_data = cipher_suite.encrypt(data.encode())
        return encrypted_data
    
    def decrypt_string(encrypted_data):
        cipher_suite = Fernet(fixed_key)
        decrypted_data = cipher_suite.decrypt(encrypted_data)
        return decrypted_data.decode()
    # Handle encryption
    def check_encrypted(data): 
        if data.startswith('g'):   
            return True
        return False 

    if check_encrypted(api_key):
        # print(f"{env_file_name} da dc ma hoa")
        decrypted_string = decrypt_string(api_key.encode())
        sudo_permission = f"echo {decrypted_string} | sudo -S"
    else:
        # print(f"{env_file_name} khong dc ma hoa")
        # encrypted_string = encrypt_string(api_key)
        sudo_permission = f"echo {api_key} | sudo -S"
    
    if sudo_permission:
        return sudo_permission
     
# Run the command line
def run_cmd(cmd):
    return run(cmd, shell=True, capture_output=True, text=True)
# 
def error_checking(status, output):
            if status != 0:
                return json.dumps({
                    'status' : False,
                    'message' : 'Error Status: ' + str(status) + ', Output: ' + output
                })
# Get tunel
def get_tunel(): 
    sudo_permission = get_secret()
    tunels = run_cmd(f"{sudo_permission} ls /etc/openvpn | grep '^server.*.conf$' | cut -d '.' -f 1")
    status_code = tunels.returncode 
    tunels_error = tunels.stderr
    if not tunels.stdout.rstrip():
        return {
            "statusCode": status_code,
            "tunels": [],
            "error": tunels_error
        }
    
    tunels_result = tunels.stdout.rstrip().split('\n')
    return {
        "statusCode": status_code,
        "tunels": tunels_result,
        "error": tunels_error
    }

# Get tunel status
def get_tunel_status(tunel):
    run_tunel_status_cmd = run_cmd(f"systemctl status openvpn@{tunel} | grep Active | awk -F' ' '{{print $2}}'")
    status = run_tunel_status_cmd.stdout.rstrip().rstrip('\n')
    return status

# Get client
def get_client():
    sudo_permission = get_secret()
    run_get_clients_cmd = run_cmd(f"{sudo_permission} tail -n +2 /etc/openvpn/easy-rsa/pki/index.txt | awk -F'=' '{{print $2}}' | sort -r")
    status_code = run_get_clients_cmd.returncode
    client_error = run_get_clients_cmd.stderr
    if not run_get_clients_cmd.stdout.rstrip():
        clients_result = []
    else:
        clients_result = run_get_clients_cmd.stdout.rstrip().split('\n')   
    # 
    run_get_clients_revoke_cmd = run_cmd(f"{sudo_permission} cat /etc/openvpn/easy-rsa/pki/index.txt | grep '^R' | awk -F'=' '{{print $2}}' | sort -r")
    if not run_get_clients_revoke_cmd.stdout.rstrip():
         clients_revoke_result = []
    else:
        clients_revoke_result = run_get_clients_revoke_cmd.stdout.rstrip().split('\n')
        
    return {
        'statusCode': status_code,
        'clientsData': clients_result,
        'clientsRevoke': clients_revoke_result,
        'error': client_error
    }

