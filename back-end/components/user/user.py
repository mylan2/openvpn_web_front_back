from flask import Blueprint, request, session
import hashlib, json
from helpers.init_helpers import connection_db

user_dp = Blueprint('user', __name__)
 
##  =========================================== API AREA ========================================================
 
@user_dp.route('/user/change-password', methods=['POST'])
def handle_change_password():
    if request.method == 'POST': 
        data = request.get_json() 
        user_name = data['username']
        old_password = data['oldPassword']
        new_password = data['newPassword']
        re_new_password = data['reNewPassword']
        if not user_name or not old_password or not new_password:
            return json.dumps({
                'status' : False,
                'message' : 'The name or password is not empty !'
            })
        if old_password == new_password:
            return json.dumps({
                'status' : False,
                'message' : 'The old password must not match the new password !'
            })
        if re_new_password != new_password:
            return json.dumps({
                'status' : False,
                'message' : 'The new password is not match !'
            })
        # Connect database
        cur, conn = connection_db()
        # 
        old_password = hashlib.md5(old_password.encode('utf-8')).hexdigest()
        select_user = "SELECT username, password FROM users WHERE username = %s and password = %s"
        user_info = (user_name , old_password)

        try:
            # Execute the update query
            cur.execute(select_user, user_info)
            result_user_excute = cur.fetchall()
            if len(result_user_excute) > 0:
                new_password = hashlib.md5(new_password.encode('utf-8')).hexdigest()
                update_password = "UPDATE users SET password = %s WHERE username = %s"
                update_password_info = (new_password, user_name)
                try:
                    cur.execute(update_password, update_password_info)
                    # Commit the changes to the database
                    conn.commit()
                except Exception as e:
                    # Rollback in case of errors
                    conn.rollback()
                    return json.dumps({
                        'status' : False,
                        'message' : str(e)
                        })
                finally:
                    # Close cursor and database connection
                    cur.close()
                    return json.dumps({
                        'status' : True,
                        'message' : 'Password Changed Successfully !'
                    })
            else:
                return json.dumps({
                'status' : False,
                'message' : 'The old password is incorrect !'
                })
  
        except Exception as e:
            return json.dumps({
                'status' : False,
                'message' : str(e)
                }) 
        finally:
            # Close cursor and database connection
            cur.close()
        
             
##  =========================================== API AREA ========================================================
 