from flask import Blueprint, request
import json, os, random, string, ipaddress
from helpers.init_helpers import connection_db, error_checking, get_secret, get_client, get_tunel, run_cmd

client_dp = Blueprint('client', __name__)
sudo_permission = get_secret() 

# API GET DATA
@client_dp.route('/get-all-client', methods=['GET'])
def handle_get_data():
    if request.method == 'GET':
        data = get_client() 
        status_code = data['statusCode']
        client_error = data['error']
        clients = data['clientsData']
        clients_revoked = data['clientsRevoke']

        if status_code != 0 or not clients:
            return json.dumps({
                'status' : False,
                'clients' : [],
                'clientConfig' : {}, 
                'message' : client_error
            })
        client_config = {}
        client_alias_name = []
        for client in clients:
            obj_temp = {}
            server = client.split('_')[1]
            ccd_folder = f"{server}.ccd"
            # 
            client_ip_cmd = f"cat /etc/openvpn/server/{ccd_folder}/{client} | awk -F' ' '{{print $2, $3}}'" 
            client_runing_status_cmd = f"{sudo_permission} cat /var/log/openvpn/openvpn-{server}-status.log | grep -m 1 {client}"
            server_update_time_cmd = f"{sudo_permission} cat /var/log/openvpn/openvpn-{server}-status.log | grep -m 1 Updated" 
            
            run_client_ip_cmd = run_cmd(client_ip_cmd)
            run_client_runing_status_cmd = run_cmd(client_runing_status_cmd)  
            run_server_update_time_cmd = run_cmd(server_update_time_cmd)  
        
            client_iP = run_client_ip_cmd.stdout.strip()
            client_runing_status = run_client_runing_status_cmd.stdout.strip().split(',')
            server_update_time = run_server_update_time_cmd.stdout.strip().split(',')

            client_name = client.split('_')[0]
            client_alias_name.append(client_name) 

            obj_temp['address'] = client_iP
            obj_temp['server'] = server
            obj_temp['fullname'] = client
            obj_temp['revoke'] = client in clients_revoked
            obj_temp['running_status'] = {
                'connected': True if len(client_runing_status) > 4 else False,
                'real_address': client_runing_status[1] if len(client_runing_status) > 1 else 'No value',
                'bytes_received': client_runing_status[2] if len(client_runing_status) > 2 else '0',
                'bytes_sent': client_runing_status[3] if len(client_runing_status) > 3 else '0',
                'connected_since': client_runing_status[4] if len(client_runing_status) > 4 else 'No value'
            }
            obj_temp['update_time'] = server_update_time[1] if len(server_update_time) > 1 else '.No value'
            client_config[client_name] = obj_temp 
        return json.dumps(
            {
                'status' : True,
                'clients' : client_alias_name,
                'clientConfig' : client_config, 
            } 
        )
# 
@client_dp.route('/client/add', methods=['POST'])
def handle_add_client():
    # url as: /client/add?name=<client>&tunel=<tunel>
    if request.method == 'POST':
        data = get_client() 
        clients = data['clientsData']
        
        # data = request.get_json()
        # if 'name' not in data:
        #     return json.dumps(
        #         { 
        #             'status' : False,
        #             'message' : "Can not defind Client name !"
        #         }) 
        
        # client_name = data['name'].strip()
        # server_name = data['tunel'] 

        client_name  = request.args.get('name', type=str , default='')
        server_name  = request.args.get('tunel',type=str , default='')
          
        if not client_name:
            return json.dumps(
                { 
                    'status' : False,
                    'message' : "Client name must not be blank !"
                })
        if not server_name:
            return json.dumps(
                { 
                    'status' : False,
                    'message' : "Tunel must not be blank !"
                })
        
        clients_alias_name = []

        for client in clients: 
            clients_alias_name.append(client.split("_")[0])
        # 
        if client_name in clients_alias_name:
            return json.dumps(
                { 
                    'status' : False,
                    'message' : "This name is already existed !"
                })
        
        # get tunel on server
        current_tunel = get_tunel()
        tunels = current_tunel['tunels']

        if not tunels:
            return json.dumps(
                { 
                    'status' : False,
                    'message' : "Can not find tunel !"
                })
       
        if server_name not in tunels:
            return json.dumps(
                { 
                    'status' : False,
                    'message' : "This tunel is not exist !"
                })
        # Create New Client 
        # Define variables
        osConfigPath = "/etc/openvpn"

        serverPortCMD = run_cmd(f"cat {osConfigPath}/{server_name}.conf | grep port | cut -d ' ' -f 2")

        if serverPortCMD.returncode != 0:
            return error_checking(serverPortCMD.returncode, f"line 140: {serverPortCMD.stderr}")
         
        if not serverPortCMD.stdout:
            return json.dumps(
                { 
                   'status' : False,
                   'message' : "Can not find server port !"
                })
        
        serverPort = ''.join(serverPortCMD.stdout.strip().split('\n'))
        currentServerCMD=run_cmd(f"{sudo_permission} ls /etc/openvpn | grep '^server.*.conf$' | cut -d '.' -f 1")
        if currentServerCMD.returncode != 0:
            return error_checking(currentServerCMD.returncode, f"line 152: {currentServerCMD.stderr}")
        
        if not currentServerCMD.stdout:
            return json.dumps(
                { 
                   'status' : False,
                   'message' : "Tunel not found in the system !"
                })
        currentServer = currentServerCMD.stdout.strip().split('\n')

        for server in currentServer:
            if server == server_name:
                # Check server.ccd folder is existed
                if os.path.exists(f"{osConfigPath}/server/{server_name}.ccd"):
                    # print("Folder is exitsed.")
                    pass
                else:
                    mkdirServerCcdFolderCMD = run_cmd(f"{sudo_permission} mkdir {osConfigPath}/server/{server_name}.ccd")
                    if mkdirServerCcdFolderCMD.returncode!= 0 :
                        return error_checking(mkdirServerCcdFolderCMD.returncode, f"line 171: {mkdirServerCcdFolderCMD.stderr}") 
                    
                # Kiem tra file server.ipp.txt co ton tai chua
                if os.path.exists(f"{osConfigPath}/server/{server_name}.ipp.txt"):
                    # print(f"File is existed.") 
                    pass
                else:
                    touchServerIppCMD =  run_cmd(f"{sudo_permission} touch {osConfigPath}/server/{server_name}.ipp.txt")
                    if touchServerIppCMD.returncode != 0 :
                        return error_checking(touchServerIppCMD.returncode, f"line 180: {touchServerIppCMD.stderr}")

        # end for
        dirIpAssign = f"{osConfigPath}/server/{server_name}.ccd"

        # Check ip availability
        # Get tunel IP
        getNetWork = run_cmd(f"cat {osConfigPath}/{server_name}.conf | grep -m 1 -w ifconfig | cut -d' ' -f 2")
        if getNetWork.returncode != 0:
            return error_checking(getNetWork.returncode, f"line 189: {getNetWork.stderr}") 
        netWork = getNetWork.stdout
    
        getNetMask = run_cmd(f"cat {osConfigPath}/{server_name}.conf | grep -m 1 -w ifconfig | cut -d' ' -f 3")
        if getNetMask.returncode!= 0:
            return error_checking(getNetMask.returncode, f"line 194: {getNetMask.stderr}") 

        netMask = ''.join(getNetMask.stdout.strip().split('\n'))
        startIP = "10" 

        # Get Client largest IP address
        # Check folder is existing
        if os.path.exists(dirIpAssign):
            # print(f"Thư mục tồn tại.")
            getFileCount = run_cmd(f"ls -l {dirIpAssign} | grep ^- | wc -l")
            if getFileCount.returncode!= 0:
                return error_checking(getFileCount.returncode, f"line 205: {getFileCount.stderr}") 
            fileCount = getFileCount.stdout 
            # Check if folder is empty
            if int(fileCount) == 0:
                # print(f"folder is empty")
                match netMask:
                    case "255.255.255.0":
                        netID = '.'.join(netWork.split('.')[:-1])
                    case "255.255.0.0":
                        netID = '.'.join(netWork.split('.')[:-2])
                    case "255.0.0.0":
                        netID = ''.join(netWork.split('.')[:-3])
                    # case _:
                    #     netID = netWork.split('.')[:-1]
                # Set default ip address starting 
                ipMAX = f"{netID}.{startIP}"
            else:
                # print(f"folder is not empty")
                # Get all item in server.ccd
                listItemsInCCDFolder =  os.listdir(dirIpAssign)
                listFileInCCDFolder = []
                # Filter item is a file in server.ccd
                for item in listItemsInCCDFolder:
                    itemPath = os.path.join(dirIpAssign, item)
                    if os.path.isfile(itemPath):
                        listFileInCCDFolder.append(item)
                # get IP address in a file already filtered
                ips = []
                for item in listFileInCCDFolder:
                    getIpInFile = run_cmd(f"cat {dirIpAssign}/{item} | cut -d ' ' -f 2")
                    if getIpInFile.returncode!= 0:
                        return error_checking(getIpInFile.returncode, f"line 236: {getIpInFile.stderr}") 
                    ips.append(getIpInFile.stdout.strip())
                # Convert IP addresses to IPv4Address objects
                ip_objects = [ipaddress.IPv4Address(ip) for ip in ips]
                ipMAX = str(max(ip_objects))

        # Declare components of IP 
        octet4 = int(ipMAX.split('.')[-1])
        octet3 = int(ipMAX.split('.')[-2])
        octet2 = int(ipMAX.split('.')[-3])
        match netMask:
            case "255.255.255.0":
                netID = '.'.join(netWork.split('.')[:-1])
                if octet4 < 254:
                    ipNext = str(octet4 + 1)
                    clientIP = f"{netID}.{ipNext}"
                else:
                    return error_checking(1, "The number of IP addresses has reached the maximum limit !")

            case "255.255.0.0":
                netID = '.'.join(netWork.split('.')[:-2])
                # 
                if octet4 < 254:
                    ipNext = str(octet4 + 1)
                    clientIP = f"{netID}.{octet3}.{ipNext}"
                # 
                elif octet3 < 254:
                    ipNext = str(octet3 + 1)
                    clientIP = f"{netID}.{ipNext}.{startIP}"
                else:
                    return error_checking(1, "The number of IP addresses has reached the maximum limit !")

            case "255.0.0.0":
                netID = ''.join(netWork.split('.')[:-3])
                 # 
                if octet4 < 254:
                    ipNext = str(octet4 + 1)
                    clientIP = f"{netID}.{octet2}.{octet3}.{ipNext}"
                # 
                elif octet3 < 254:
                    ipNext = str(octet3 + 1)
                    clientIP = f"{netID}.{octet2}.{ipNext}.{startIP}"
                # 
                elif octet2 < 254:
                    ipNext = str(octet2 + 1)
                    if octet3 > 253:
                        octet3 = "0"
                    clientIP = f"{netID}.{ipNext}.{octet3}.{startIP}" 
                else:
                    return error_checking(1, "The number of IP addresses has reached the maximum limit !")
        
        # Random string of name client
        def random_string(length):
            letters = string.ascii_letters
            return ''.join(random.choice(letters) for _ in range(length))
        randomString = random_string(10)

        client_name = f"{client_name}_{server_name}_{randomString}"
        if os.path.exists(f"{osConfigPath}/client/{client_name}"):
            print(f"{osConfigPath}/client/{client_name} ton tai")
            pass
        else:
            mkdirClientFolderCMD =  run_cmd(f"{sudo_permission} mkdir {osConfigPath}/client/{client_name}")
            if mkdirClientFolderCMD.returncode!= 0 :
                return error_checking(mkdirClientFolderCMD.returncode, f"line 302: {mkdirClientFolderCMD.stderr}")

        genClientKeyCMD = run_cmd(f"{sudo_permission} sh -c 'cd {osConfigPath}/easy-rsa/ && ./easyrsa --batch --days=7300 build-client-full {client_name} nopass'")
 
        if genClientKeyCMD.returncode != 0 :
            return error_checking(genClientKeyCMD.returncode, f"line 311: {genClientKeyCMD.stderr}") 

        # copy client cert and client key to folder client
        # Check client cert and client key files exist
        if run_cmd(f"{sudo_permission} ls {osConfigPath}/easy-rsa/pki/issued/{client_name}.crt").returncode == 0:
            copyCrtFileCMD =  run_cmd(f"{sudo_permission} cp {osConfigPath}/easy-rsa/pki/issued/{client_name}.crt {osConfigPath}/client/{client_name}/")
            if copyCrtFileCMD.returncode != 0:
                return error_checking(copyCrtFileCMD.returncode, f"line 318: {copyCrtFileCMD.stderr}")
        else:
            return error_checking(1, f"{client_name}.crt is not existing !")
        # 
        if run_cmd(f"{sudo_permission} ls {osConfigPath}/easy-rsa/pki/private/{client_name}.key").returncode == 0:
            copyPrivateKeyCMD = run_cmd(f"{sudo_permission} cp {osConfigPath}/easy-rsa/pki/private/{client_name}.key {osConfigPath}/client/{client_name}/")
            if copyPrivateKeyCMD.returncode!= 0:
                return error_checking(copyPrivateKeyCMD.returncode, f"line 325: {copyPrivateKeyCMD.stderr}") 
        else:
            return error_checking(1, f"{client_name}.key is not existing !")
       
        # # Create client config file ovpn
        touchClientOvpnFileCMD = run_cmd(f"{sudo_permission} touch {osConfigPath}/client/{client_name}/{client_name}.ovpn")
      
        if touchClientOvpnFileCMD.returncode != 0:
            return error_checking(touchClientOvpnFileCMD.returncode, f"line 333: {touchClientOvpnFileCMD.stderr}") 

        # 
        # Mở hoặc tạo một tập tin 
        serverPublicIP = "192.168.188.144"
        clientKeyPath = f"{osConfigPath}/client/{client_name}"
        serverKeyPath = f"{osConfigPath}/server/"
        cipher = "AES-256-GCM"
        serverCA = run_cmd(f"{sudo_permission} cat {serverKeyPath}/ca.crt").stdout
        clientCert = run_cmd(f"{sudo_permission} cat {clientKeyPath}/{client_name}.crt").stdout
        clientKey = run_cmd(f"{sudo_permission} cat {clientKeyPath}/{client_name}.key").stdout
        tlsCrypt = run_cmd(f"{sudo_permission} cat {serverKeyPath}/ta.key").stdout

        clientOvpnFilePath = f'{osConfigPath}/client/{client_name}/{client_name}.ovpn'
        # Write multi text lines to file
        clientConfigText = f"""client
dev tun
proto udp
remote {serverPublicIP} {serverPort}
resolv-retry infinite
nobind
user nobody
group nogroup
persist-key
persist-tun
remote-cert-tls server
cipher {cipher} 
verb 3
<ca>
{serverCA}
</ca>
<cert>
{clientCert}
</cert>
<key>
{clientKey}
</key>
<tls-crypt>
{tlsCrypt}
</tls-crypt> 
"""
            
        writeClientConfigText_cmd = f"{sudo_permission} python3 -c 'with open(\"{clientOvpnFilePath}\", \"w\") as file: file.write(\"\"\"{clientConfigText}\"\"\")'"
        runWriteClientConfigText_cmd = run_cmd(writeClientConfigText_cmd)
        if runWriteClientConfigText_cmd.returncode!= 0:
            return error_checking(runWriteClientConfigText_cmd.returncode, f"line 378: {runWriteClientConfigText_cmd.stderr}")
      
        #  
        if os.path.exists(f"{dirIpAssign}/{client_name}"):
            print(f"{dirIpAssign}/{client_name} ton tai")
            pass
        else:
            touchClientIpAssignFileCMD =  run_cmd(f"{sudo_permission} touch {dirIpAssign}/{client_name}") 
            if touchClientIpAssignFileCMD.returncode != 0:
                return error_checking(touchClientIpAssignFileCMD.returncode, f"line 383: {touchClientIpAssignFileCMD.stderr}") 
            
            clientAssignIP = f"""ifconfig-push {clientIP} {netMask}""" 
            
            echoClientIPAssign_cmd = f"{sudo_permission} python3 -c 'with open(\"{dirIpAssign}/{client_name}\", \"w\") as file: file.write(\"\"\"{clientAssignIP}\"\"\")'"
            runEchoClientIPAssign_cmd = run_cmd(echoClientIPAssign_cmd)

            if runEchoClientIPAssign_cmd.returncode != 0:
                return error_checking(runEchoClientIPAssign_cmd.returncode, f"line 391: {runEchoClientIPAssign_cmd.stderr}") 
        
        # If all is successfully
        # Insert client data to database
        # Connect database
        cur, conn = connection_db()
        # 
        select_server = "SELECT server_id FROM servers WHERE server_name = %s;"
        server_info = (server_name,)
         
        try:
            # Execute the update query
            cur.execute(select_server, server_info)
            result_server_excute = cur.fetchall()
            if len(result_server_excute) > 0:
                server_id = result_server_excute[0][0]
                insert_client = "INSERT INTO clients (client_id, client_name, client_key, client_cert, client_config, server_id) VALUES (%s, %s, %s, %s, %s, %s);"   
                try:
                    cur.execute(insert_client, (None, client_name, clientKey, clientCert, clientConfigText, server_id))
                    # Commit the changes to the database
                    conn.commit()
                    cur.close()
                    conn.close()
                except Exception as e:
                    # Rollback in case of errors
                    conn.rollback() 
                finally:
                    # Close cursor and database connection
                    cur.close()
            else:
                cur.close()
                conn.close()
                
        except Exception as e:
            print(f"Database execute error: {str(e)}")
        finally:
            # Close cursor and database connection
            cur.close()  
        # 
        return json.dumps({ 
                'status' : True,
                'message' : "Add client successfully !"
            })
 
# 
@client_dp.route('/client/<client>/profile', methods=['GET'])
def handle_get_profile(client):
    if request.method == 'GET':
        # data = request.get_json()
        # client = data['client']
        # client = data['client']
        data = get_client()
        clients = data['clientsData']

        if client not in clients:
            return json.dumps({
              'status' : False,
              'message' : 'This client is not exist !'
            })
        # 
        get_profile_cmd = f"cat /etc/openvpn/client/{client}/{client}.ovpn"
        run_get_profile_cmd = run_cmd(get_profile_cmd)
        status_code = run_get_profile_cmd.returncode 
        result_error = run_get_profile_cmd.stderr
        result_output = run_get_profile_cmd.stdout

        if status_code != 0:
            return error_checking(status_code, result_error) 
        return json.dumps({
            'status' : True,
            'profile': result_output.rstrip()
        })
#   
@client_dp.route('/client/<client>/delete', methods=['DELETE'])
def handle_delete_client(client): 
    if request.method == 'DELETE':
    #   data = request.get_json()
        # client_name_to_delete = data['client']  
        client_name_to_delete = client 
        data = get_client()
        clients = data['clientsData'] 
        server = client_name_to_delete.split('_')[1]

        if client_name_to_delete not in clients:
            return json.dumps({
              'status' : False,
              'message' : 'This client is not exist !'
            })
        # Delete the client in index file
        del_client_in_index_file_cmd = f"{sudo_permission} sed -i '/{client_name_to_delete}/d' /etc/openvpn/easy-rsa/pki/index.txt"
        del_client_in_req_file_cmd = f"{sudo_permission} rm -f /etc/openvpn/easy-rsa/pki/reqs/{client_name_to_delete}.req"
        del_client_key_file_cmd = f"{sudo_permission} rm -f /etc/openvpn/easy-rsa/pki/private/{client_name_to_delete}.key"
        del_client_cert_file_cmd = f"{sudo_permission} rm -f /etc/openvpn/easy-rsa/pki/issued/{client_name_to_delete}.crt"
        del_client_ip_config_file_cmd = f"{sudo_permission} rm -f /etc/openvpn/server/{server}.ccd/{client_name_to_delete}"
        del_client_config_folder_cmd = f"{sudo_permission} rm -rf /etc/openvpn/client/{client_name_to_delete}"

        del_in_index_file = run_cmd(del_client_in_index_file_cmd) 
        if del_in_index_file.returncode != 0:
            return error_checking(del_in_index_file.returncode, del_in_index_file.stderr) 
        
        if run_cmd(f"{sudo_permission} ls /etc/openvpn/easy-rsa/pki/reqs/{client_name_to_delete}.req").returncode == 0:
            del_in_req_file = run_cmd(del_client_in_req_file_cmd) 
            if del_in_req_file.returncode != 0:
                return error_checking(del_in_req_file.returncode, del_in_req_file.stderr) 
       
        if run_cmd(f"{sudo_permission} ls /etc/openvpn/easy-rsa/pki/private/{client_name_to_delete}.key").returncode == 0:
            del_key_file = run_cmd(del_client_key_file_cmd) 
            if del_key_file.returncode != 0:
                return error_checking(del_key_file.returncode, del_key_file.stderr) 
        
        if run_cmd(f"{sudo_permission} ls /etc/openvpn/easy-rsa/pki/issued/{client_name_to_delete}.crt").returncode == 0:
            del_cert_file = run_cmd(del_client_cert_file_cmd) 
            if del_cert_file.returncode != 0:
                return error_checking(del_cert_file.returncode, del_cert_file.stderr)
        
        if os.path.exists(f"/etc/openvpn/server/{server}.ccd/{client_name_to_delete}"):
            del_ip_config_file = run_cmd(del_client_ip_config_file_cmd)
            if del_ip_config_file.returncode != 0:
                return error_checking(del_ip_config_file.returncode, del_ip_config_file.stderr)

        if os.path.exists(f"/etc/openvpn/client/{client_name_to_delete}"): 
            del_config_folder = run_cmd(del_client_config_folder_cmd) 
            if del_config_folder.returncode != 0:
                return error_checking(del_config_folder.returncode, del_config_folder.stderr)
        
        # Delete from database 
        # Connect database
        cur, conn = connection_db()
        #   
        try: 
            update_query = "UPDATE clients SET client_status = %s WHERE client_name = %s;"
            data_to_update = (1, client_name_to_delete)
            cur.execute(update_query, data_to_update)
            conn.commit()
            cur.close()
            conn.close()
        except Exception as e:
            conn.rollback() 
            print('delete ' + str(e))

        return json.dumps({
            'status' : True,
            'message': f"Successfully deleted {client_name_to_delete.split('_')[0]} !"
        })
#   
@client_dp.route('/client/<client>/revoke', methods=['POST'])
def handle_revoke_client(client):
    if request.method == 'POST':
    #     data = request.get_json()
    #     client_name_to_revoke = data['client'] 
        client_name_to_revoke = client
        data = get_client()
        clients = data['clientsData'] 
        clients_revoked = data['clientsRevoke']
        server = client_name_to_revoke.split('_')[1]

        if client_name_to_revoke not in clients:
            return json.dumps({
                'status' : False,
                'message' : 'This client is not exist !'
            })
        if client_name_to_revoke in clients_revoked:
            return json.dumps({
                'status' : False,
                'message' : 'This client is already revoked !'
            })
        # Delete the client in index file 
        op_config_path = '/etc/openvpn' 
        op_easyrsa_config_path = '/etc/openvpn/easy-rsa/' 

        run_revoke_client_cmd = run_cmd(f"{sudo_permission} sh -c 'cd {op_easyrsa_config_path} && ./easyrsa --batch revoke {client_name_to_revoke}'")
 
        if run_revoke_client_cmd.returncode != 0:
            return json.dumps({
             'status' : False,
             'message' : f"Failed to Revoke {client_name_to_revoke.split('_')[0]}"
            })

        run_regen_crl_file = run_cmd(f"{sudo_permission} sh -c 'cd {op_easyrsa_config_path} && ./easyrsa --batch --days=7300 gen-crl'")

        if run_regen_crl_file.returncode!= 0:
            return json.dumps({
            'status' : False,
            'message' : "Failed to ReGen Crl file !"
            })
        
        run_remove_old_crl_file_cmd = run_cmd(f"{sudo_permission} rm -f {op_config_path}/server/crl.pem")

        if run_remove_old_crl_file_cmd.returncode!= 0:
            return json.dumps({
                'status' : False,
                'message' : "Failed to Remove Old Crl File !"
            })
        
        run_cp_new_crl_file_cmd = run_cmd(f"{sudo_permission} cp {op_config_path}/easy-rsa/pki/crl.pem {op_config_path}/server/crl.pem")
        
        if run_cp_new_crl_file_cmd.returncode!= 0:
            return json.dumps({
               'status' : False,
               'message' : "Failed to Copy New Crl File !"
            })
        
        run_restart_openvpn_service_cmd = run_cmd(f"{sudo_permission} systemctl restart openvpn@{server}.service")

        if run_restart_openvpn_service_cmd.returncode!= 0:
            return json.dumps({
              'status' : False,
              'message' : "Failed to ReStart OpenVPN Service !"
            })
        # 
        # Revoke from database 
        # Connect database
        cur, conn = connection_db()
        #   
        try: 
            update_query = "UPDATE clients SET client_revoked = %s WHERE client_name = %s;"
            data_to_update = (1, client_name_to_revoke)
            cur.execute(update_query, data_to_update)
            conn.commit()
            cur.close()
            conn.close()
        except Exception as e:
            conn.rollback() 
            print('Revoke: ' + str(e))

        return json.dumps({
            'status' : True,
            'message': f"Successfully revoked {client_name_to_revoke.split('_')[0]} !"
        })
# 